// ============================================================================
// JigParser
// Simple parser to pick apart the Jiggle command strings.
//
// CLH April-June 1998
// ============================================================================

package com.chrishobson.jiggle;

public class JigParser {

  // ==========================================================================
  // Create a parser for the given string.
  // ==========================================================================
  public JigParser(String s) {
    reset(s);
  }

  // ==========================================================================
  // Allow an empty constructor for ease of use.
  // ==========================================================================
  public JigParser() {
  }

  // ==========================================================================
  // Reset parser to allow a new string to be parsed, avoids the need to keep
  // newing a parser for each string.
  // ==========================================================================
  public void reset(String s) {
    m_string = s;
    m_last = s.length() - 1;
    m_index = -1;
  }

  // ==========================================================================
  // Returns true whilst there is more available.
  // ==========================================================================
  public boolean more() {
    return m_index < m_last;
  }

  // ==========================================================================
  // Returns true if next item is a character.
  // ==========================================================================
  public boolean is_char() {
    char c = m_string.charAt(m_index + 1);

    return c < '0' || c > '9';
  }

  // ==========================================================================
  // Moves to next int and returns it. Returns -1 if no more.
  // ==========================================================================
  public int next_int() {
    int val = -1;

    while (++m_index <= m_last) {
      char c = m_string.charAt(m_index);
      if (c >= '0' && c <= '9') {
        // If this is the first digit initialize m_val to zero
        // so that it gets correctly set to the value of the digit
        if (val < 0)
          val = 0;
        // Multiple m_val by 10 and add the new digit
        val = val * 10 + (int) (c - '0');
      } else if (val > -1) {
        // Break out if we have parsed an integer and now have found
        // more none integer data. Step back one to ensure we point
        // at the character for next is_char() and next_char()
        --m_index;
        break;
      }
    }
    return val;
  }

  // ==========================================================================
  // Returns the next character.
  // ==========================================================================
  public char next_char() {
    return m_string.charAt(++m_index);
  }

  // ==========================================================================
  // Private member data
  // ==========================================================================

  private String m_string; // The string
  private int m_last; // Index of last character in m_string
  private int m_index; // current character index
}
