package com.chrishobson.jiggle;

//
//  Small application to test the JigWorm class
//

import com.chrishobson.jiggle.JigSocket;
import java.net.*;

class TestJigServer {
  public static void main(String args[]) throws Exception {
    System.out.println("JigWorm Server Harness");

    Socket sock = null;
    try {
      // Try to open a connection to the game server
      sock = new Socket("localhost", 1964);
    } catch (Exception e) {
      System.out.println("Cannot connect to server");
      System.exit(0);
    }

    JigSocket js = new JigSocket(sock);
    js.send("D");

    String l = null;
    while ((l = js.get()) == null && js.ok()) {
      System.out.println("Yield");
      Thread.yield();
    }
    if (l != null) {
      System.out.println(l);
    } else {
      System.out.println("Lost connection");
    }

  }

}
