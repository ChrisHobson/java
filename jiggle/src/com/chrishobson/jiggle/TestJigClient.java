//
//  Small application to test the JigClient class
//
package com.chrishobson.jiggle;

import com.chrishobson.jiggle.JigClient;
import com.chrishobson.jiggle.JigSocket;

import java.net.*;

class TestJigClient {
  public static void main(String args[]) throws Exception {
    System.out.println("JigClient Test Harness");

    JigSocket sock = null;
    try {
      // Try to open a connection to the game server
      sock = new JigSocket(new Socket("zappa", 1964));
    } catch (Exception e) {
      System.out.println("Cannot connect to server");
      System.exit(0);
    }

    // Create the JigClient
    new JigClient(sock, 10, 10, 10).start();
  }
}
