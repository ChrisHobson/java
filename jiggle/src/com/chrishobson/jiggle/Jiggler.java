// ============================================================================
// Jiggler
// The Jiggle client requires a main program, Jiggler supplies the main
// A Jiggler is someone who wants to play Jiggle.
//
// CLH April-June 1998
// ============================================================================

package com.chrishobson.jiggle;

import java.net.*;

class Jiggler {

  // ==========================================================================
  // The jigglers main.
  // ==========================================================================
  public static void main(String args[]) throws Exception {
    // Create a unamed Jiggler. The constructor is the program
    new Jiggler(args);
  }

  private Jiggler(String args[]) {
    System.out.println("Jiggle. A Multi-player game by Chris Hobson");
    System.out.println("===========================================");

    Socket s = null;
    try {
      // Try to open a connection to the game server. localhost is used
      // because the University only have zappa. This could easily be
      // made a command line argument. See JigGame for an example of
      // parsing arguments.
      s = new Socket("localhost", 1964);
    } catch (Exception e) {
      System.out.println("Cannot connect to server");
      System.exit(0);
    }

    // Create the socket communication class
    JigSocket m_sock = new JigSocket(s);

    // Get the X Y size of the playing area from the game server
    // By sending the server the Dimension command
    m_sock.send("D");

    // Perform a blocking wait for the reply
    String sdim = m_sock.get_blocking();
    if (sdim == null) {
      bye("Failed to get playing area size from server");
    }
    // And the Id of this Jiggler connection
    m_sock.send("I");
    String sid = m_sock.get_blocking();
    if (sid == null) {
      bye("Failed to get Jiggler ID from server");
    }
    // Parse the dimension string
    JigParser parser = new JigParser(sdim);
    int x = parser.next_int();
    int y = parser.next_int();
    int num_food = parser.next_int();
    int num_blocks = parser.next_int();
    parser.reset(sid);
    int id = parser.next_int();

    System.out.println("Started Jiggler ID " + id + " game size is " + x
        + " by " + y);
    System.out.println("There are " + num_food + " apples and " + num_blocks
        + " walls");
    // Create the client which performs the communication from
    // now on, set the client thread running
    JigClient client = new JigClient(m_sock, id, num_food, num_blocks);
    client.start();

    // Create the graphics which display the game and talk to
    // the server via the JigClient, and set them running
    new JigGraphics(client, x, y).start();

    // We've finished. Java will continue to run whilst there are
    // active threads so we can just return and leave the
    // threads to do their work.
  }

  // ==========================================================================
  // A tidy way out if an error occurs during startup
  // ==========================================================================
  private void bye(String reason) {
    System.out.println("Jiggle Terminated....");
    System.out.println(reason);
    System.exit(0);
  }

}
