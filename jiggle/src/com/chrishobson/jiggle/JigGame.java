// ============================================================================
// JigGame
// The Jiggle game requires a main program, JigGame supplies the main
//
// CLH April-June 1998
// ============================================================================

package com.chrishobson.jiggle;

import java.io.*;
import java.net.*;

public class JigGame {

  // ==========================================================================
  // The game. It is the server side 'Main' program
  // ==========================================================================
  public static void main(String args[]) throws IOException {

    System.out.println("Jiggle. A Multi-player game by Chris Hobson");
    System.out.println("===========================================");
    parse_args(args);

    // The main program creates the necessary classes and
    // listens for connections. It doesn't do anything else
    // the rest is handled by the classes
    JigServer server = new JigServer(m_x, m_y, m_max_food, m_max_block);
    server.start();

    // Server socket to listen for connections on
    ServerSocket serversock = new ServerSocket(1964, 6);
    // Allow a timeout of 1 millisecond before yielding control
    // that is if nothing is waiting to connect don't hang around
    serversock.setSoTimeout(1);
    // Should 'Waiting' message be printed ?
    boolean print = true;
    // Now simply loop forever awaiting connection from new players
    while (true) {
      Socket sock = null;
      boolean timeout = false;
      if (print == true) {
        System.out.println("Waiting for a new player....");
      }
      try {
        sock = serversock.accept();
      } catch (InterruptedIOException e) {
        // Timed out so set flag
        timeout = true;
        // Don't print 'Waiting' message again
        print = false;
        // There was nothing to connect so sleep for a couple of
        // seconds, connections are low priority
        try {
          Thread.sleep(2000);
        } catch (Exception e2) {
        }
      }
      if (timeout == false) {
        System.out.println("Player accepted, initialising them");
        server.add(sock);
        // Print 'waiting' next time through
        print = true;
      }
    }
  }

  // ==========================================================================
  // Method to parse any command line arguments
  // ==========================================================================
  static private void parse_args(String args[]) {
    // Arguments come in arg, value pairs
    // loop through and handle any we know
    // ignore anything that looks suspect
    int i = 0;
    try {
      for (; i < args.length - 1; i += 2) {
        if (args[i].equals("-x")) {
          m_x = Integer.parseInt(args[i + 1]);
        } else if (args[i].equals("-y")) {
          m_y = Integer.parseInt(args[i + 1]);
        } else if (args[i].equals("-f")) {
          m_max_food = Integer.parseInt(args[i + 1]);
        } else if (args[i].equals("-b")) {
          m_max_block = Integer.parseInt(args[i + 1]);
        }
      }
    } catch (NumberFormatException e) {
      System.out.println("Error parsing value for " + args[i]);
      System.out.println(args[i + 1] + " should be an integer");
      System.exit(1);
    }
  }

  // The defaults if no command line arguments given
  static private int m_x = 20;
  static private int m_y = 20;
  static private int m_max_food = 5;
  static private int m_max_block = 10;
}
