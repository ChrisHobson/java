package com.chrishobson.jiggle;
import com.chrishobson.jiggle.JigParser;

class TestIntParser {
  public static void main(String args[]) {
    JigParser p = new JigParser(args[0]);

    while(p.more()) {
      if(p.is_char()) {
	System.out.println(p.next_char());
      } else {
	System.out.println(p.next_int());
      }
    }
  }
}
