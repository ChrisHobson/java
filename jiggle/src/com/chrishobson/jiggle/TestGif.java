package com.chrishobson.jiggle;

import java.awt.*;

public class TestGif {
  static public void main(String args[]) {
    Image i = Toolkit.getDefaultToolkit().getImage(args[0]);

    if (i == null) {
      System.out.println("Failed to load " + args[0]);
      System.exit(1);
    }
    i = i.getScaledInstance(16, 16, Image.SCALE_DEFAULT);

    Image i2 = Toolkit.getDefaultToolkit().getImage(args[1]);
    if (i2 == null) {
      System.out.println("Failed to load " + args[1]);
      System.exit(1);
    }
    i2 = i2.getScaledInstance(16, 16, Image.SCALE_DEFAULT);

    Frame f = new Frame("Food Food...");
    myCanvas mc = new myCanvas(i);
    f.setSize(200, 200);
    f.add(mc);
    f.setVisible(true);
    // show();

    while (true) {
      mc.setImage(i2);
      mc.setImage(i);
    }

  }
}

class myCanvas extends Canvas {
  boolean painted = false;
  Image m_i;
  int m_x = 10;
  int m_y = 10;
  Image m_off_i;
  Graphics m_off_g;

  public myCanvas(Image i) {
    this.setSize(200, 200);
    m_i = i;
  }

  public void paint(Graphics g) {
    // System.out.println("Error why is paint called?");
    // System.exit(0);
  }

  public void update(Graphics g) {
    if (m_off_i == null) {
      m_off_i = createImage(200, 200);
      m_off_g = m_off_i.getGraphics();
      m_off_g.setColor(Color.gray);
      m_off_g.fillRect(0, 0, 200, 200);
    }
    for (int i = 0; i < 160; i += 16) {
      m_off_g.drawImage(m_i, m_x + i, m_y, this);
    }
    g.drawImage(m_off_i, 0, 0, this);
    painted = true;
  }

  public void setImage(Image i) {
    m_i = i;
    painted = false;
    repaint();
    while (painted == false) {
      try {
        Thread.sleep(5);
      } catch (Exception e) {
      }
      if (painted == false) {
        System.out.println("Still waiting");
      }
    }
    m_x += 160;
    if (m_x > 170) {
      m_x = 10;
      m_y += 16;
      ;
      if (m_y > 170) {
        try {
          Thread.sleep(50000);
        } catch (Exception e) {
        }

      }
    }
  }
}
