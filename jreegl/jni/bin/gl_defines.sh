#!/bin/sh
#
# Script to extract the #defines from opengl and format
# them as java static variables.
#
egrep '^ *#define *GL_' 'c:/Program Files/Microsoft SDK/include/gl/gl.h' | \
 sed -e 's/^ *#define *//' \
     -e 's/ / = /' \
     -e 's/$/;/' \
     -e 's/^/public static int /'
