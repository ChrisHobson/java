#include "JavaString.h"

JavaString::JavaString(
  JNIEnv *a_Env
, jstring a_Str
)
: m_Env(a_Env)
, m_Str(a_Str)
{
  m_Chars = m_Env->GetStringUTFChars(a_Str, NULL);
}

JavaString::operator const char*() const
{
  return m_Chars;
}

JavaString::~JavaString()
{
  m_Env->ReleaseStringUTFChars(m_Str, m_Chars);
}