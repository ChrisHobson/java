#include "GL/glut.h"
#include "com_chrishobson_jreegl_gl.h"

/*
 * Class:     com_chrishobson_jreegl_gl
 * Method:    Clear
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_com_chrishobson_jreegl_gl_glClear(
  JNIEnv *
, jclass
, jint a_Flags
)
{
  glClear(a_Flags);
}


/*
 * Class:     com_chrishobson_jreegl_gl
 * Method:    ClearColor
 * Signature: (DDDD)V
 */
JNIEXPORT void JNICALL Java_com_chrishobson_jreegl_gl_glClearColor(
  JNIEnv *
, jclass
, jdouble r
, jdouble g
, jdouble b
, jdouble a
)
{
  glClearColor((float)r,(float)g,(float)b,(float)a);
}

/*
 * Class:     com_chrishobson_jreegl_gl
 * Method:    glColor
 * Signature: (DDD)V
 */
JNIEXPORT void JNICALL Java_com_chrishobson_jreegl_gl_glColor(
  JNIEnv*
, jclass
, jdouble a_R
, jdouble a_G
, jdouble a_B
)
{
  glColor3d(a_R, a_G, a_B);
}

/*
 * Class:     com_chrishobson_jreegl_gl
 * Method:    glShadeModel
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_com_chrishobson_jreegl_gl_glShadeModel
  (JNIEnv *, jclass, jint a_Mode)
{
  glShadeModel(a_Mode);
}


/*
 * Class:     com_chrishobson_jreegl_gl
 * Method:    glOrtho
 * Signature: (DDDDDD)V
 */
JNIEXPORT void JNICALL Java_com_chrishobson_jreegl_gl_glOrtho(
  JNIEnv *
, jclass
, jdouble a_left
, jdouble a_right
, jdouble a_bottom
, jdouble a_top
, jdouble a_near
, jdouble a_far
)
{
  glOrtho(a_left, a_right, a_bottom, a_top, a_near, a_far);
}

/*
 * Class:     com_chrishobson_jreegl_gl
 * Method:    glFrustum
 * Signature: (DDDDDD)V
 */
JNIEXPORT void JNICALL Java_com_chrishobson_jreegl_gl_glFrustum
  (JNIEnv *, jclass, jdouble left, jdouble right
  , jdouble bottom, jdouble top, jdouble a_near, jdouble a_far)
{
  glFrustum(left, right, bottom, top, a_near, a_far);
}

/*
 * Class:     com_chrishobson_jreegl_gl
 * Method:    gluPerspective
 * Signature: (DDDD)V
 */
JNIEXPORT void JNICALL Java_com_chrishobson_jreegl_gl_gluPerspective
  (JNIEnv *, jclass, jdouble fovy, jdouble aspect, jdouble a_near, jdouble a_far)
{
  gluPerspective(fovy, aspect, a_near, a_far);
}

/*
 * Class:     com_chrishobson_jreegl_gl
 * Method:    glBegin
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_com_chrishobson_jreegl_gl_glBegin(
  JNIEnv *
, jclass
, jint a_Begin
)
{
  glBegin(a_Begin);
}

/*
 * Class:     com_chrishobson_jreegl_gl
 * Method:    glVertex
 * Signature: (DDD)V
 */
JNIEXPORT void JNICALL Java_com_chrishobson_jreegl_gl_glVertex(
  JNIEnv *
, jclass
, jdouble x
, jdouble y
, jdouble z
)
{
  glVertex3d(x,y,z);
}
/*
 * Class:     com_chrishobson_jreegl_gl
 * Method:    glEnd
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_com_chrishobson_jreegl_gl_glEnd(
  JNIEnv *
, jclass
)
{
  glEnd();
}

/*
 * Class:     com_chrishobson_jreegl_gl
 * Method:    glFlush
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_com_chrishobson_jreegl_gl_glFlush(
  JNIEnv *
, jclass
)
{
  glFlush();
}

/*
 * Class:     com_chrishobson_jreegl_gl
 * Method:    glViewport
 * Signature: (IIII)V
 */
JNIEXPORT void JNICALL Java_com_chrishobson_jreegl_gl_glViewport(
  JNIEnv *
, jclass
, jint x
, jint y
, jint width
, jint height
)
{
  glViewport(x, y, width, height);
}

/*
 * Class:     com_chrishobson_jreegl_gl
 * Method:    glMatrixMode
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_com_chrishobson_jreegl_gl_glMatrixMode(
  JNIEnv *
, jclass
, jint a_Mode
)
{
  glMatrixMode(a_Mode);
}
/*
 * Class:     com_chrishobson_jreegl_gl
 * Method:    glPushMatrix
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_com_chrishobson_jreegl_gl_glPushMatrix
  (JNIEnv *, jclass)
{
  glPushMatrix();
}

/*
 * Class:     com_chrishobson_jreegl_gl
 * Method:    glPopMatrix
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_com_chrishobson_jreegl_gl_glPopMatrix
  (JNIEnv *, jclass)
{
  glPopMatrix();
}


/*
 * Class:     com_chrishobson_jreegl_gl
 * Method:    glLoadIdentity
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_com_chrishobson_jreegl_gl_glLoadIdentity(
  JNIEnv *
, jclass
)
{
  glLoadIdentity();
}

/*
 * Class:     com_chrishobson_jreegl_gl
 * Method:    gluLookAt
 * Signature: (DDDDDDDDD)V
 */
JNIEXPORT void JNICALL Java_com_chrishobson_jreegl_gl_gluLookAt(
  JNIEnv *, jclass, 
  jdouble eyex
, jdouble eyey
, jdouble eyez
, jdouble centrex 
, jdouble centrey
, jdouble centrez
, jdouble upx
, jdouble upy
, jdouble upz
)
{
  gluLookAt(eyex, eyey, eyez, centrex, centrey, centrez, upx, upy, upz);
}

/*
 * Class:     com_chrishobson_jreegl_gl
 * Method:    glRotate
 * Signature: (DDDD)V
 */
JNIEXPORT void JNICALL Java_com_chrishobson_jreegl_gl_glRotate(
  JNIEnv *, jclass, 
  jdouble angle
, jdouble x
, jdouble y
, jdouble z
)
{
  glRotated(angle,x,y,z);
}

/*
 * Class:     com_chrishobson_jreegl_gl
 * Method:    glScale
 * Signature: (DDD)V
 */
JNIEXPORT void JNICALL Java_com_chrishobson_jreegl_gl_glScale(
  JNIEnv *, jclass, 
  jdouble x
, jdouble y
, jdouble z
)
{
  glScaled(x,y,z);
}

/*
 * Class:     com_chrishobson_jreegl_gl
 * Method:    glTranslate
 * Signature: (DDD)V
 */
JNIEXPORT void JNICALL Java_com_chrishobson_jreegl_gl_glTranslate(
  JNIEnv *, jclass, 
  jdouble x
, jdouble y
, jdouble z
)
{
  glTranslated(x,y,z);
}

/*
 * Class:     com_chrishobson_jreegl_gl
 * Method:    glGenLists
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_com_chrishobson_jreegl_gl_glGenLists
  (JNIEnv *, jclass, jint num)
{
  return glGenLists(num);
}

/*
 * Class:     com_chrishobson_jreegl_gl
 * Method:    glNewList
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_com_chrishobson_jreegl_gl_glNewList
  (JNIEnv *, jclass, jint id, jint mode)
{
  glNewList(id, mode);
}

/*
 * Class:     com_chrishobson_jreegl_gl
 * Method:    glEndList
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_com_chrishobson_jreegl_gl_glEndList
  (JNIEnv *, jclass)
{
  glEndList();
}

/*
 * Class:     com_chrishobson_jreegl_gl
 * Method:    glCallList
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_com_chrishobson_jreegl_gl_glCallList
  (JNIEnv *, jclass, jint id)
{
  glCallList(id);
}

/*
 * Class:     com_chrishobson_jreegl_gl
 * Method:    glDeleteLists
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_com_chrishobson_jreegl_gl_glDeleteLists
  (JNIEnv *, jclass, jint id, jint num)
{
  glDeleteLists(id, num);
}
