/*
 * A wrapper which converts a jstring into a const char* for passing
 * to C. When the JavaString goes out of scope destructor ensures that
 * any JVM resources are de-allocated
 */

#include <jni.h>

class JavaString {
public:
  JavaString(JNIEnv* a_Env, jstring a_Str);
  ~JavaString();

  operator const char*() const;

private:
  JNIEnv* m_Env;
  jstring m_Str;
  const char* m_Chars;
};