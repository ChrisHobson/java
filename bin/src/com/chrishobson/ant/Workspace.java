package com.chrishobson.ant;
import org.apache.tools.ant.types.Path;
import org.apache.tools.ant.*;
import java.io.*;
public class Workspace extends Task
{
  public Workspace()
  {
  }
  @Override
  public void execute()
  {
    Project l_Proj = getProject();
    String l_ProjDir = l_Proj.getProperty("basedir");
    
    try {
      FileWriter l_cp = new FileWriter(l_ProjDir + "/.classpath");
      FileWriter l_p = new FileWriter(l_ProjDir + "/.project");
      l_cp.write(
        "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
        "<classpath>\n" +
        "  <classpathentry kind=\"con\" path=\"org.eclipse.jdt.launching.JRE_CONTAINER\"/>\n"
      );
      l_p.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
      "<projectDescription>\n" +
      "  <name>" + l_Proj.getName() + "</name>\n" +
      "  <comment></comment>\n" +
      "  <projects>\n" +
      "  </projects>\n" +
      "  <buildSpec>\n" +
      "    <buildCommand>\n" +
      "      <name>org.eclipse.jdt.core.javabuilder</name>\n" +
      "      <arguments>\n" +
      "      </arguments>\n" +
      "    </buildCommand>\n" +
      "  </buildSpec>\n" +
      "  <natures>\n" +
      "    <nature>org.eclipse.jdt.core.javanature</nature>\n" +
      "  </natures>\n" +
      "  <linkedResources>\n"
      );

      for(String l_Path : m_Path.list()) {
        if(l_Path.endsWith(".jar")){
          l_cp.write("  <classpathentry kind=\"lib\" path=\"" + l_Path + "\"/>\n");
        } else {
          File l_F = new File(l_Path);
          File l_P = l_F.getParentFile();
          String l_Name = l_P.getName();
          // Do not output the main project as a linked src as
          // it will be called "src" in the project
          if(l_P.toString().equals(l_ProjDir)) {
            // Tree has src (normally will) so add directly rather than linked
            l_cp.write("  <classpathentry kind=\"src\" path=\"src\"/>\n");
          } else {
            l_cp.write("  <classpathentry kind=\"src\" path=\"" + l_Name + "\"/>\n");
          
            l_p.write(
            "  <link>\n" +
            "    <name>" + l_Name + "</name>\n" +
            "    <type>2</type>\n" +
            "    <location>" + l_Path.replace('\\','/') + "</location>\n" +
            "  </link>\n"
            );
          }
        }
      }
      l_cp.write(
        "  <classpathentry kind=\"output\" path=\"" + m_Output + "\"/>\n" +
        "</classpath>\n"
      );
      l_p.write(
      "  </linkedResources>\n" +
      "</projectDescription>\n"
      );
      l_cp.close();
      l_p.close();
    } catch(Exception e) {
      e.printStackTrace();
    }
  }
  public void setOutput(
    String a_Output
  )
  {
    m_Output = a_Output;
  }
  public void addConfiguredPath(
    Path a_Path
  )
  {
    m_Path = a_Path;
  }
  
  private Path m_Path;
  private String m_Output = "classes";
}
