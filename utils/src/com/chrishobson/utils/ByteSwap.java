package com.chrishobson.utils;

public class ByteSwap {
  public static short swap(short s) {
    int ch1 = ((s & 0xff00) >>> 8);
    int ch2 = ((s & 0xff) << 8);

    return (short)(ch2 | ch1);
  }

  public static int swap(int i) {
    int ch1 = ((i & 0xff000000) >>> 24);
    int ch2 = ((i & 0x00ff0000) >> 16);
    int ch3 = ((i & 0x0000ff00) >> 8);
    int ch4 = (i & 0x000000ff);

    return (ch4 << 24) + (ch3 << 16) + (ch2 << 8) + ch1;
  }

  // Convert a float to integer bits, swap the integer and
  // then convert its bits back to a float

  public static float swap(float f) {
    return Float.intBitsToFloat(swap(Float.floatToIntBits(f)));
  }

}
