#ifndef one_h
#define one_h

class one {
public:
  one() { std::cout << "One calling\n"; }
  ~one() { std::cout << "One going away\n"; }
};

#endif
