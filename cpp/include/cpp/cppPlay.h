//=============================================================================
//D Base class for all C++ playing/examples/experiments
//
// ----------------------------------------------------------------------------
// Copyright 2013 Delcam plc., Birmingham, UK
// ----------------------------------------------------------------------------
//
// History.
// DICC   Who When     What   
// ------ --- -------- --------------------------------------------------------
// 999999 CLH 18/06/13 Written.
//-----------------------------------------------------------------------------

#ifndef _cppPlay_H
#define _cppPlay_H

// includes from 'cpp' library
#include "porting.h"
// includes from PowerINSPECT libraries
// includes from Delcam libraries
#include "utils/utCheckValid.h"

// includes from other libraries
// system includes

// class predeclarations to avoid header file inclusion

// types: classes, enums, typedefs
// variables: consts, statics, exported variables (declared extern elsewhere)
// local forward function declarations

//=============================================================================
class cppPlay { 
public:

  cppPlay();
  // Constructor.

  virtual ~cppPlay();
  // Destructor.

  virtual void run() = 0;
  // The entry point of the example
  
protected:

  // functions
  // variables

private:

  // friends

  // functions
  cppPlay(const cppPlay&);
  void operator=(const cppPlay&);
  // Memberwise copying is prohibited.

  // variables

  UT_CHECKVALID;
};

#endif

