//=============================================================================
//D Shows how named constructors work
//
// Imagine a date class that can be constructed in uk dd/mm/yy format or
// USA mm/dd/yy format, but always prints as dd/mm/yy. How can we
// pass 3 integers to a constructor and tell it the format?
// 
// A boolean you shout
// 
// Date myData(12,5,64,true); // UK
// Date myData(5,12,64, false); // USA
// 
// Yuk, you cannot deduce anything useful from the code. You could use
// an enum for the format instead of a boolean
// 
// enum dateFormat {
//   xxDMY,
//   xxMDY
// };
// Date myData(xxDMY, 12, 5, 64); // UK
// Date myData(xxMDY  5, 12, 64,); // USA
// 
// But now the code inside has to switch based on the enum, imagine you have
// lots of different ways to construct an instance this would be messy.
// 
// The trick is to use two separate enums so you can call a physically
// different constructor, each enum as ONE and only ONE enumerator
// 
// enum xxDateUSA {
//   xxMDY
//  };
//
// enum xxDateUK {
//   xxDMY
//  };
//  
//  Calling looks much the same BUT there are now two constructors, and you
//  can still tell what form of instance is being constructed from reading 
//  the code, the constructor is "named".
//  
// ----------------------------------------------------------------------------
// Copyright 2013 Delcam plc., Birmingham, UK
// ----------------------------------------------------------------------------
//
// History.
// DICC   Who When     What   
// ------ --- -------- --------------------------------------------------------
// 999999 CLH 19/06/13 Written.
//-----------------------------------------------------------------------------

#ifndef _cppNamedConstructor_H
#define _cppNamedConstructor_H

// includes from 'cpp' library
#include "cpp/cppPlay.h"
// includes from PowerINSPECT libraries
// includes from Delcam libraries
#include "utils/utCheckValid.h"

// includes from other libraries
// system includes

// class predeclarations to avoid header file inclusion

// types: classes, enums, typedefs
enum DateDMY {
  xxDMY
};
enum DateMDY {
  xxMDY
};
// variables: consts, statics, exported variables (declared extern elsewhere)
// local forward function declarations

//=============================================================================
class cppNamedConstructor : public cppPlay { 
public:
  cppNamedConstructor();
  // Default constructor to allow run() to be called, so the class can be
  // both the date example and a date


  cppNamedConstructor(DateDMY, int day, int month, int year);
  cppNamedConstructor(DateMDY, int month, int day, int year);
  // Named constructors

  ~cppNamedConstructor();
  // Destructor.

  void print() const;
  // Prints date in UK D/M/Y format

  virtual void run() override;

protected:

  // functions
  // variables

private:

  // friends

  // functions
  cppNamedConstructor(const cppNamedConstructor&);
  void operator=(const cppNamedConstructor&);
  // Memberwise copying is prohibited.

  // variables
  int m_day;
  int m_month;
  int m_year;

  UT_CHECKVALID;
};

#endif

