#ifndef template1_h
#define template1_h
//
// Simple template
// Use as
// T1<int> tint(12, "Integer %d\r\n");
// tint.print();
// T1<double> tdouble(1.23, "Double %f\r\n");
// tdouble.print();
// 
template<class t_X>
class T1
{
public:
  // Constructor which takes a value and a format string which
  // is used by print();
  T1(t_X value, char* format);
 
  // Destructor
  ~T1();

  // Prints the value using the format string
  void print() const;

private:
  t_X m_value;
  char* m_format;
};

#include "play_headers/template1_t.h"

#endif