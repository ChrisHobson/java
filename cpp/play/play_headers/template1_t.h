//
// Simple template implementation
// 
template<class t_X>
T1<t_X>::T1(t_X value, char* format)
  : m_value(value),
    m_format(format)
{
}

template<class t_X>
T1<t_X>::~T1()
{
}

template<class t_X>
void T1<t_X>::print() const
{
  printf(m_format, m_value);
}
