//=============================================================================
//D Show auto keyword can lead to unexpected types dangerous
//
// ----------------------------------------------------------------------------
// Copyright 2013 Delcam plc., Birmingham, UK
// ----------------------------------------------------------------------------
//
// History.
// DICC   Who When     What   
// ------ --- -------- --------------------------------------------------------
// 999999 CLH 28/06/13 Written.
//-----------------------------------------------------------------------------

#ifndef _cppAutoKeyword_H
#define _cppAutoKeyword_H

// includes from 'cpp' library
#include "cpp/cppPlay.h"
// includes from PowerINSPECT libraries
// includes from Delcam libraries
#include "utils/utCheckValid.h"

// includes from other libraries
// system includes

// class predeclarations to avoid header file inclusion

// types: classes, enums, typedefs
// variables: consts, statics, exported variables (declared extern elsewhere)
// local forward function declarations

//=============================================================================
class cppAutoKeyword : public cppPlay { 
public:

  cppAutoKeyword();
  // Constructor.

  ~cppAutoKeyword();
  // Destructor.

  cppAutoKeyword(const cppAutoKeyword&);
  cppAutoKeyword& operator=(const cppAutoKeyword&);

  const cppAutoKeyword& get() const;
  // Returns *this

  void run() override;

  void none_const();

protected:

  // functions
  // variables

private:

  // friends

  // functions
  // Memberwise copying is prohibited.

  // variables

  UT_CHECKVALID;
};

#endif

