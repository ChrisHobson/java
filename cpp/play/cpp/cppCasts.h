//=============================================================================
//D Inline boolean cast example to show simpler solution than templates
//
// ----------------------------------------------------------------------------
// Copyright 2013 Delcam plc., Birmingham, UK
// ----------------------------------------------------------------------------
//
// History.
// DICC   Who When     What   
// ------ --- -------- --------------------------------------------------------
// 999999 CLH 18/06/13 Written.
//-----------------------------------------------------------------------------

#ifndef _cppCasts_H
#define _cppCasts_H

// includes from 'cpp' library
#include "cpp/cppPlay.h"
// includes from PowerINSPECT libraries
// includes from Delcam libraries
#include "utils/utCheckValid.h"

// includes from other libraries
// system includes

// class predeclarations to avoid header file inclusion

// types: classes, enums, typedefs
// variables: consts, statics, exported variables (declared extern elsewhere)
// local forward function declarations

//=============================================================================
class cppCasts : public cppPlay { 
public:

  cppCasts();
  // Constructor.

  virtual ~cppCasts();
  // Destructor.

  virtual void run() override;

protected:

  // functions
  // variables

private:

  // friends

  // functions
  cppCasts(const cppCasts&);
  void operator=(const cppCasts&);
  // Memberwise copying is prohibited.

  // variables

  UT_CHECKVALID;
};

#include <windows.h>
typedef short VARIANT_BOOL;

typedef int clhBOOL; // pretend to be utBool / erBOOL
typedef int clhBOOL; // pretend to be utBool / erBOOL

// This works for utBool, erBOOL (clhBOOL), BOOL, VARIANT_BOOL
// as all are basically int (or short in the case of VARIANT_BOOL)
inline bool to_bool(int b) { 
  return !!b;
}

// Going the other way it make sense to have converters which name
// the type even though a single function would work for
// utBool, erBOOL (clhBOOL) and BOOL as they are all int with 1 as true
// 
// How ever the one function will convert bool and also ANY of the
// other types, as C++ automatically promotes a bool to an int
// 
inline BOOL to_BOOL(int b) {
  return b ? TRUE : FALSE;
}

inline clhBOOL to_clhBool(int b) {
  return to_BOOL(b);
}

inline VARIANT_BOOL to_VARIANT_BOOL(int b) { 
  // Must use Microsoft defines as TRUE is -1
  return b ? VARIANT_TRUE : VARIANT_FALSE;
}

// Handy utility to compare any type of booleans for equality
inline bool bool_equal(int a, int b) {
  return (!!a) == (!!b);
}


#endif

