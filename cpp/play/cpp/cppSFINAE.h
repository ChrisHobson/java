//=============================================================================
//D <One line description>
//
// <Details of class and its usage from user viewpoint>
//
// ----------------------------------------------------------------------------
// Copyright 2013 Delcam plc., Birmingham, UK
// ----------------------------------------------------------------------------
//
// History.
// DICC   Who When     What   
// ------ --- -------- --------------------------------------------------------
// 999999 CLH 18/09/13 Written.
//-----------------------------------------------------------------------------

#ifndef _cppSFINAE_H
#define _cppSFINAE_H

// includes from 'cpp' library
#include "cpp/cppPlay.h"
// includes from PowerINSPECT libraries
// includes from Delcam libraries
#include "utils/utCheckValid.h"
// includes from other libraries
// system includes

// class predeclarations to avoid header file inclusion

// types: classes, enums, typedefs
// variables: consts, statics, exported variables (declared extern elsewhere)
// local forward function declarations

//=============================================================================
class cppSFINAE : public cppPlay{
public:

  cppSFINAE();
  // Constructor.

  ~cppSFINAE();
  // Destructor.

  virtual void run() override;

protected:

  // functions
  // variables

private:

  // friends

  // functions
  cppSFINAE(const cppSFINAE&);
  void operator=(const cppSFINAE&);
  // Memberwise copying is prohibited.

  // variables

  UT_CHECKVALID;
};

#endif
