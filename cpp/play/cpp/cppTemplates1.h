//=============================================================================
//D Simple play with templates
//
// ----------------------------------------------------------------------------
// Copyright 2013 Delcam plc., Birmingham, UK
// ----------------------------------------------------------------------------
//
// History.
// DICC   Who When     What   
// ------ --- -------- --------------------------------------------------------
// 999999 CLH 18/06/13 Written.
//-----------------------------------------------------------------------------

#ifndef _cppTemplates1_H
#define _cppTemplates1_H

// includes from 'cpp' library
#include "cpp/cppPlay.h"
#include "play_headers/template1.h"

// includes from PowerINSPECT libraries
// includes from Delcam libraries
#include "utils/utCheckValid.h"

// includes from other libraries
// system includes

// class predeclarations to avoid header file inclusion

// types: classes, enums, typedefs
// variables: consts, statics, exported variables (declared extern elsewhere)
// local forward function declarations
  extern template class T1<float>;

//=============================================================================
class cppTemplates1 : public cppPlay { 
public:

  cppTemplates1();
  // Constructor.

  ~cppTemplates1();
  // Destructor.

  virtual void run() override;

protected:

  // functions
  // variables

private:

  // friends

  // functions
  cppTemplates1(const cppTemplates1&);
  void operator=(const cppTemplates1&);
  // Memberwise copying is prohibited.

  // variables

  T1<float> m_floats;

  UT_CHECKVALID;
};

#endif

