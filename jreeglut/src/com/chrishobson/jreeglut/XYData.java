package com.chrishobson.jreeglut;

/**
 * Base class for HandlerData where the event includes an XY. This
 * Data class also provides access to any modifier keys that were
 * in effect when the event was sent, it does this efficiently by
 * only calling glutGetModifiers once regardless of the number of
 * calls to the query functions.
 * 
 * @author clh
 * 
 */
public class XYData extends HandlerData {

  public XYData(
      glutWindow a_Wind, int a_X, int a_Y
  )
  {
    super(a_Wind);
    m_X = a_X;
    m_Y = a_Y;
  }
  
  public int GetX()
  {
    return m_X;
  }

  public int GetY()
  {
    return m_Y;
  }
  
  private int m_X;
  private int m_Y;
}
