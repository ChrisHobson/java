package com.chrishobson.jreeglut;

/**
 * Java interface for handlers for glut mouse button events.
 * 
 * @author clh
 * 
 */
public interface MouseHandler {

  /**
   * This function is called by glut when a mouse button is clicked
   * in a window. The handler is not called if a menu is attached
   * to the button.
   * 
   * @param a_Window
   * @param button
   * @param state
   * @param x
   * @param y
   */
  public void MouseFunc(MouseData a_Data);
}
