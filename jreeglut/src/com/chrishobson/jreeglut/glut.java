package com.chrishobson.jreeglut;

/**
 * Java wrapper to the freeglut library. This class wraps freeglut using a JNI
 * library jreeglut. Only functions provided by freeglut are provided, those that
 * can be directly called have the same name as in glut, for example
 * glutInit. Those which cannot be directly called because they take
 * Java types have the glut prefix removed and would normally be called
 * indirectly by other java code.
 * <p>
 * The cleanest way (in my opinion) to use the functions is to 
 * statically import the glut class, this allows direct calling
 * without prefixing with a class name. The same applies to using
 * glut contants.
 * <p>
 * <b>import static  com.chrishobson.jreeglut.glut.*;</b>
 * 
 * @author clh
 * 
 */
public class glut {

  static public native void glutInit(String[] a_Args);
  
  static public native void glutInitDisplayMode(int a_Mode);
  
  static public native void glutInitWindowSize(int x, int y);
  static public native void glutInitWindowPosition(int x, int y);

  static public native int glutCreateWindow(String a_Name);
  
  static public native int glutCreateMenu();
  
  static public native void glutSetMenu(int a_Menu);
  
  static public native void glutAddMenuEntry(String a_Text, int a_Item);
  
  static public native void glutAttachMenu(Class<glutMenu> a_Class, int a_Button);
  
  static public native void glutAddSubMenu(String a_Text, int a_Id);

  static public native void glutMainLoop();
  
  static public native void glutPostRedisplay();
  
  static public native void glutSetWindow(int a_Id);
  
  static public native void glutSwapBuffers();
  
  static public native void glutWireCube(double size);
  
  static public native void glutWireSphere(double radius, int slices, int stacks);
  
  static public native int glutGetModifiers();
  
  static public native long wglGetCurrentContext();
  
  static public native boolean wglShareLists(long main, long other);

  /**
   * Causes the MotionFunc callback to be setup. This is used by the glutWindow
   * to hook in to the callback the first time glutWindow.SetMotionHandler is
   * used.
   */
  static protected native void MotionFunc(Class<glutWindow> a_Class);

  /**
   * Causes the PassiveMotionFunc callback to be setup. This is used by the
   * glutWindow to hook in to the callback the first time
   * glutWindow.SetPassiveMotionHandler is used.
   */
  static protected native void PassiveMotionFunc(Class<glutWindow> a_Class);
  
  /**
   * Causes the ReshapeFunction callback to be setup.
   */
  static protected native void ReshapeFunc(Class<glutWindow> a_Class);

  /**
   * Causes the DisplayFunction callback to be setup.
   */
  static protected native void DisplayFunc(Class<glutWindow> a_Class);
  
  /**
   * Causes the KeyboardFunc callback to be setup
   */
  static protected native void KeyboardFunc(Class<glutWindow> a_Class);

  /**
   * Causes the SpecialFunc callback to be setup
   */
  static protected native void SpecialFunc(Class<glutWindow> a_Class);

  /**
   * Causes the MouseFunc callback to be setup
   */
  static protected native void MouseFunc(Class<glutWindow> a_Class);
  
  public static int  GLUT_KEY_F1                       = 0x0001;
  public static int  GLUT_KEY_F2                       = 0x0002;
  public static int  GLUT_KEY_F3                       = 0x0003;
  public static int  GLUT_KEY_F4                       = 0x0004;
  public static int  GLUT_KEY_F5                       = 0x0005;
  public static int  GLUT_KEY_F6                       = 0x0006;
  public static int  GLUT_KEY_F7                       = 0x0007;
  public static int  GLUT_KEY_F8                       = 0x0008;
  public static int  GLUT_KEY_F9                       = 0x0009;
  public static int  GLUT_KEY_F10                      = 0x000A;
  public static int  GLUT_KEY_F11                      = 0x000B;
  public static int  GLUT_KEY_F12                      = 0x000C;
  public static int  GLUT_KEY_LEFT                     = 0x0064;
  public static int  GLUT_KEY_UP                       = 0x0065;
  public static int  GLUT_KEY_RIGHT                    = 0x0066;
  public static int  GLUT_KEY_DOWN                     = 0x0067;
  public static int  GLUT_KEY_PAGE_UP                  = 0x0068;
  public static int  GLUT_KEY_PAGE_DOWN                = 0x0069;
  public static int  GLUT_KEY_HOME                     = 0x006A;
  public static int  GLUT_KEY_END                      = 0x006B;
  public static int  GLUT_KEY_INSERT                   = 0x006C;
  public static int  GLUT_LEFT_BUTTON                  = 0x0000;
  public static int  GLUT_MIDDLE_BUTTON                = 0x0001;
  public static int  GLUT_RIGHT_BUTTON                 = 0x0002;
  public static int  GLUT_DOWN                         = 0x0000;
  public static int  GLUT_UP                           = 0x0001;
  public static int  GLUT_LEFT                         = 0x0000;
  public static int  GLUT_ENTERED                      = 0x0001;
  public static int  GLUT_RGB                          = 0x0000;
  public static int  GLUT_RGBA                         = 0x0000;
  public static int  GLUT_INDEX                        = 0x0001;
  public static int  GLUT_SINGLE                       = 0x0000;
  public static int  GLUT_DOUBLE                       = 0x0002;
  public static int  GLUT_ACCUM                        = 0x0004;
  public static int  GLUT_ALPHA                        = 0x0008;
  public static int  GLUT_DEPTH                        = 0x0010;
  public static int  GLUT_STENCIL                      = 0x0020;
  public static int  GLUT_MULTISAMPLE                  = 0x0080;
  public static int  GLUT_STEREO                       = 0x0100;
  public static int  GLUT_LUMINANCE                    = 0x0200;
  public static int  GLUT_MENU_NOT_IN_USE              = 0x0000;
  public static int  GLUT_MENU_IN_USE                  = 0x0001;
  public static int  GLUT_NOT_VISIBLE                  = 0x0000;
  public static int  GLUT_VISIBLE                      = 0x0001;
  public static int  GLUT_HIDDEN                       = 0x0000;
  public static int  GLUT_FULLY_RETAINED               = 0x0001;
  public static int  GLUT_PARTIALLY_RETAINED           = 0x0002;
  public static int  GLUT_FULLY_COVERED                = 0x0003;
  public static int  GLUT_WINDOW_X                     = 0x0064;
  public static int  GLUT_WINDOW_Y                     = 0x0065;
  public static int  GLUT_WINDOW_WIDTH                 = 0x0066;
  public static int  GLUT_WINDOW_HEIGHT                = 0x0067;
  public static int  GLUT_WINDOW_BUFFER_SIZE           = 0x0068;
  public static int  GLUT_WINDOW_STENCIL_SIZE          = 0x0069;
  public static int  GLUT_WINDOW_DEPTH_SIZE            = 0x006A;
  public static int  GLUT_WINDOW_RED_SIZE              = 0x006B;
  public static int  GLUT_WINDOW_GREEN_SIZE            = 0x006C;
  public static int  GLUT_WINDOW_BLUE_SIZE             = 0x006D;
  public static int  GLUT_WINDOW_ALPHA_SIZE            = 0x006E;
  public static int  GLUT_WINDOW_ACCUM_RED_SIZE        = 0x006F;
  public static int  GLUT_WINDOW_ACCUM_GREEN_SIZE      = 0x0070;
  public static int  GLUT_WINDOW_ACCUM_BLUE_SIZE       = 0x0071;
  public static int  GLUT_WINDOW_ACCUM_ALPHA_SIZE      = 0x0072;
  public static int  GLUT_WINDOW_DOUBLEBUFFER          = 0x0073;
  public static int  GLUT_WINDOW_RGBA                  = 0x0074;
  public static int  GLUT_WINDOW_PARENT                = 0x0075;
  public static int  GLUT_WINDOW_NUM_CHILDREN          = 0x0076;
  public static int  GLUT_WINDOW_COLORMAP_SIZE         = 0x0077;
  public static int  GLUT_WINDOW_NUM_SAMPLES           = 0x0078;
  public static int  GLUT_WINDOW_STEREO                = 0x0079;
  public static int  GLUT_WINDOW_CURSOR                = 0x007A;
  public static int  GLUT_SCREEN_WIDTH                 = 0x00C8;
  public static int  GLUT_SCREEN_HEIGHT                = 0x00C9;
  public static int  GLUT_SCREEN_WIDTH_MM              = 0x00CA;
  public static int  GLUT_SCREEN_HEIGHT_MM             = 0x00CB;
  public static int  GLUT_MENU_NUM_ITEMS               = 0x012C;
  public static int  GLUT_DISPLAY_MODE_POSSIBLE        = 0x0190;
  public static int  GLUT_INIT_WINDOW_X                = 0x01F4;
  public static int  GLUT_INIT_WINDOW_Y                = 0x01F5;
  public static int  GLUT_INIT_WINDOW_WIDTH            = 0x01F6;
  public static int  GLUT_INIT_WINDOW_HEIGHT           = 0x01F7;
  public static int  GLUT_INIT_DISPLAY_MODE            = 0x01F8;
  public static int  GLUT_ELAPSED_TIME                 = 0x02BC;
  public static int  GLUT_WINDOW_FORMAT_ID             = 0x007B;
  public static int  GLUT_HAS_KEYBOARD                 = 0x0258;
  public static int  GLUT_HAS_MOUSE                    = 0x0259;
  public static int  GLUT_HAS_SPACEBALL                = 0x025A;
  public static int  GLUT_HAS_DIAL_AND_BUTTON_BOX      = 0x025B;
  public static int  GLUT_HAS_TABLET                   = 0x025C;
  public static int  GLUT_NUM_MOUSE_BUTTONS            = 0x025D;
  public static int  GLUT_NUM_SPACEBALL_BUTTONS        = 0x025E;
  public static int  GLUT_NUM_BUTTON_BOX_BUTTONS       = 0x025F;
  public static int  GLUT_NUM_DIALS                    = 0x0260;
  public static int  GLUT_NUM_TABLET_BUTTONS           = 0x0261;
  public static int  GLUT_DEVICE_IGNORE_KEY_REPEAT     = 0x0262;
  public static int  GLUT_DEVICE_KEY_REPEAT            = 0x0263;
  public static int  GLUT_HAS_JOYSTICK                 = 0x0264;
  public static int  GLUT_OWNS_JOYSTICK                = 0x0265;
  public static int  GLUT_JOYSTICK_BUTTONS             = 0x0266;
  public static int  GLUT_JOYSTICK_AXES                = 0x0267;
  public static int  GLUT_JOYSTICK_POLL_RATE           = 0x0268;
  public static int  GLUT_OVERLAY_POSSIBLE             = 0x0320;
  public static int  GLUT_LAYER_IN_USE                 = 0x0321;
  public static int  GLUT_HAS_OVERLAY                  = 0x0322;
  public static int  GLUT_TRANSPARENT_INDEX            = 0x0323;
  public static int  GLUT_NORMAL_DAMAGED               = 0x0324;
  public static int  GLUT_OVERLAY_DAMAGED              = 0x0325;
  public static int  GLUT_VIDEO_RESIZE_POSSIBLE        = 0x0384;
  public static int  GLUT_VIDEO_RESIZE_IN_USE          = 0x0385;
  public static int  GLUT_VIDEO_RESIZE_X_DELTA         = 0x0386;
  public static int  GLUT_VIDEO_RESIZE_Y_DELTA         = 0x0387;
  public static int  GLUT_VIDEO_RESIZE_WIDTH_DELTA     = 0x0388;
  public static int  GLUT_VIDEO_RESIZE_HEIGHT_DELTA    = 0x0389;
  public static int  GLUT_VIDEO_RESIZE_X               = 0x038A;
  public static int  GLUT_VIDEO_RESIZE_Y               = 0x038B;
  public static int  GLUT_VIDEO_RESIZE_WIDTH           = 0x038C;
  public static int  GLUT_VIDEO_RESIZE_HEIGHT          = 0x038D;
  public static int  GLUT_NORMAL                       = 0x0000;
  public static int  GLUT_OVERLAY                      = 0x0001;
  public static int  GLUT_ACTIVE_SHIFT                 = 0x0001;
  public static int  GLUT_ACTIVE_CTRL                  = 0x0002;
  public static int  GLUT_ACTIVE_ALT                   = 0x0004;
  public static int  GLUT_CURSOR_RIGHT_ARROW           = 0x0000;
  public static int  GLUT_CURSOR_LEFT_ARROW            = 0x0001;
  public static int  GLUT_CURSOR_INFO                  = 0x0002;
  public static int  GLUT_CURSOR_DESTROY               = 0x0003;
  public static int  GLUT_CURSOR_HELP                  = 0x0004;
  public static int  GLUT_CURSOR_CYCLE                 = 0x0005;
  public static int  GLUT_CURSOR_SPRAY                 = 0x0006;
  public static int  GLUT_CURSOR_WAIT                  = 0x0007;
  public static int  GLUT_CURSOR_TEXT                  = 0x0008;
  public static int  GLUT_CURSOR_CROSSHAIR             = 0x0009;
  public static int  GLUT_CURSOR_UP_DOWN               = 0x000A;
  public static int  GLUT_CURSOR_LEFT_RIGHT            = 0x000B;
  public static int  GLUT_CURSOR_TOP_SIDE              = 0x000C;
  public static int  GLUT_CURSOR_BOTTOM_SIDE           = 0x000D;
  public static int  GLUT_CURSOR_LEFT_SIDE             = 0x000E;
  public static int  GLUT_CURSOR_RIGHT_SIDE            = 0x000F;
  public static int  GLUT_CURSOR_TOP_LEFT_CORNER       = 0x0010;
  public static int  GLUT_CURSOR_TOP_RIGHT_CORNER      = 0x0011;
  public static int  GLUT_CURSOR_BOTTOM_RIGHT_CORNER   = 0x0012;
  public static int  GLUT_CURSOR_BOTTOM_LEFT_CORNER    = 0x0013;
  public static int  GLUT_CURSOR_INHERIT               = 0x0064;
  public static int  GLUT_CURSOR_NONE                  = 0x0065;
  public static int  GLUT_CURSOR_FULL_CROSSHAIR        = 0x0066;
  public static int  GLUT_RED                          = 0x0000;
  public static int  GLUT_GREEN                        = 0x0001;
  public static int  GLUT_BLUE                         = 0x0002;
  public static int  GLUT_KEY_REPEAT_OFF               = 0x0000;
  public static int  GLUT_KEY_REPEAT_ON                = 0x0001;
  public static int  GLUT_KEY_REPEAT_DEFAULT           = 0x0002;
  public static int  GLUT_JOYSTICK_BUTTON_A            = 0x0001;
  public static int  GLUT_JOYSTICK_BUTTON_B            = 0x0002;
  public static int  GLUT_JOYSTICK_BUTTON_C            = 0x0004;
  public static int  GLUT_JOYSTICK_BUTTON_D            = 0x0008;
  public static int  GLUT_GAME_MODE_ACTIVE             = 0x0000;
  public static int  GLUT_GAME_MODE_POSSIBLE           = 0x0001;
  public static int  GLUT_GAME_MODE_WIDTH              = 0x0002;
  public static int  GLUT_GAME_MODE_HEIGHT             = 0x0003;
  public static int  GLUT_GAME_MODE_PIXEL_DEPTH        = 0x0004;
  public static int  GLUT_GAME_MODE_REFRESH_RATE       = 0x0005;
  public static int  GLUT_GAME_MODE_DISPLAY_CHANGED    = 0x0006;

  static {
    System.loadLibrary("freeglut");
    System.loadLibrary("jreeglut");
  }
}
