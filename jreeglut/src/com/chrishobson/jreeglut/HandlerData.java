package com.chrishobson.jreeglut;

/**
 * All handlers take a HandlerData or a class derived from it. The
 * base class stores the glutWindow involved in the event, derived classes
 * add more data specific to the event type.
 * 
 * @author clh
 * 
 */
public class HandlerData {

  public HandlerData(
      glutWindow a_Wind
  )
  {
    m_Wind = a_Wind;
  }
  
  public glutWindow GetWindow()
  {
    return m_Wind;
  }
  
  private glutWindow m_Wind;
  
}
