package com.chrishobson.jreeglut;

/**
 * All handlers take a HandlerData or a class derived from it. The
 * base class stores the glutWindow involved in the event, derived classes
 * add more data specific to the event type.
 * 
 * @author clh
 * 
 */
public class MouseData extends XYWithModData {

  public MouseData(
      glutWindow a_Wind, int a_But, int a_State, int a_X, int a_Y
  )
  {
    super(a_Wind, a_X, a_Y);
    m_But = a_But;
    m_State = a_State;
  }
  
  public int GetBut()
  {
    return m_But;
  }

  public boolean IsDown()
  {
    return m_State == 1;
  }
  
  private int m_But;
  private int m_State;
}
