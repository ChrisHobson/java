package com.chrishobson.jreeglut;

/**
 * All handlers take a HandlerData or a class derived from it. The
 * base class stores the glutWindow involved in the event, derived classes
 * add more data specific to the event type.
 * 
 * @author clh
 * 
 */
public class KeyboardData extends XYWithModData {

  public KeyboardData(
      glutWindow a_Wind, int a_Key, int a_X, int a_Y
  )
  {
    super(a_Wind, a_X, a_Y);
    m_Key = a_Key;
  }
  
  public int GetKey()
  {
    return m_Key;
  }
  
  private int m_Key;
}
