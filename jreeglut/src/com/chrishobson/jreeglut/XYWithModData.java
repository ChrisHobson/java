package com.chrishobson.jreeglut;

import static com.chrishobson.jreeglut.glut.*;

/**
 * Base class for HandlerData where the event includes an XY. This
 * Data class also provides access to any modifier keys that were
 * in effect when the event was sent, it does this efficiently by
 * only calling glutGetModifiers once regardless of the number of
 * calls to the query functions.
 * 
 * @author clh
 * 
 */
public class XYWithModData extends XYData {

  public XYWithModData(
      glutWindow a_Wind, int a_X, int a_Y
  )
  {
    super(a_Wind, a_X, a_Y);
  }
  
  /**
   * Returns all glut modifier keys that were pressed when the event
   * occured. It may be clearer to call the Isxxx methods which wrap
   * this function.
   * @return The glut modifiers 
   */
  public int GetModifiers()
  {
    if(!m_CalledMods) {
      m_Mods = glutGetModifiers();
      m_CalledMods = true;
    }
    return m_Mods;
  }

  /**
   * Was the event generated with the shift key down.
   * @return True if shift
   */
  public boolean IsShift()
  {
    return (GetModifiers() & GLUT_ACTIVE_SHIFT) != 0;
  }

  /**
   * Was the event generated with the alt key down.
   * @return True if shift
   */
  public boolean IsAlt()
  {
    return (GetModifiers() & GLUT_ACTIVE_ALT) != 0;
  }

  /**
   * Was the event generated with the ctrl key down.
   * @return True if shift
   */
  public boolean IsCtrl()
  {
    return (GetModifiers() & GLUT_ACTIVE_CTRL) != 0;
  }
  
  boolean m_CalledMods;
  int m_Mods;
}
