package com.chrishobson.jreeglut;

/**
 * Java interface for handlers for glut Menu event.
 * 
 * @author clh
 * 
 */
public interface MenuHandler {

  /**
   * This function is called by glut when a menu is selected in the window that
   * this handler this handler is attached to. The window is passed to the
   * handler.
   * 
   */
  public void MenuFunc(glutWindow a_Window, MenuData a_Data);
}
