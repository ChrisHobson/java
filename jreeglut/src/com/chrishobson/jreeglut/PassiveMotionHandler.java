package com.chrishobson.jreeglut;

/**
 * Java interface for handlers for glut passive motion.
 * 
 * @author clh
 * 
 */
public interface PassiveMotionHandler {

  /**
   * This function is called by glut when passive motion occurs within the
   * Window that this handler is attached to. The window is passed to the
   * handler, which allows it to determine which window is involved, allow the
   * same handler to be attached to multiple windows if desired.
   * 
   * @param x
   * @param y
   */
  public void PassiveMotionFunc(PassiveMotionData a_Data);
}
