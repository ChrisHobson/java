package com.chrishobson.jreeglut;

/**
 * Data for Passive Motion events.
 * 
 * @author clh
 * 
 */
public class PassiveMotionData extends XYWithModData {

  public PassiveMotionData(
      glutWindow a_Wind, int a_X, int a_Y
  )
  {
    super(a_Wind, a_X, a_Y);
  }
}
