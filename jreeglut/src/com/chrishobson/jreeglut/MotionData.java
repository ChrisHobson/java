package com.chrishobson.jreeglut;

/**
 * Data for Motion events.
 * 
 * @author clh
 * 
 */
public class MotionData extends XYWithModData {

  public MotionData(
      glutWindow a_Wind, int a_X, int a_Y
  )
  {
    super(a_Wind, a_X, a_Y);
  }
}
