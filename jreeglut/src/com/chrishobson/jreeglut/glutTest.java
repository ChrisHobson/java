package com.chrishobson.jreeglut;

import static com.chrishobson.jreeglut.glut.*;

/**
 * Absolute minimal jreeglut test program. jreeglut does not provide an OpenGL
 * wrappers and so cannot do much in terms of graphics. However the fact we can
 * open a window is enough to prove that the jreeglut JNI library has been found
 * and that the JNI can connect to the freeglut library and that the freeglut
 * library can make OpenGL calls.
 * 
 * @author clh
 * 
 */
public class glutTest {

  public static void main(String[] a_Args) {
    glutInit(a_Args);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_DEPTH | GLUT_RGBA);
    glutWindow l_W = new glutWindow("Window");
    WindowHandler l_H = new WindowHandler();
    l_W.SetMotionHandler(l_H);
    l_W.SetPassiveMotionHandler(l_H);
    l_W.SetReshapeHandler(l_H);
    l_W.SetDisplayHandler(l_H);
    l_W.SetKeyboardHandler(l_H);
    l_W.SetMenuHandler(l_H);

    glutMenu l_Menu = new glutMenu();
    l_Menu.AddEntry("One", null);
    l_Menu.AddEntry("Two", null);

    l_W.AttachMenu(l_Menu, GLUT_LEFT_BUTTON);

    glutMenu l_Menu2 = new glutMenu();
    l_Menu2.AddEntry("Three", null);
    l_Menu2.AddEntry("Four", null);

    l_Menu.AddSubMenu("Sub", l_Menu2);

    glutMainLoop();
  }

}

class WindowHandler implements MotionHandler, ReshapeHandler, DisplayHandler,
    PassiveMotionHandler, KeyboardHandler, MenuHandler {
  public void MotionFunc(MotionData a_Data) {
    System.out.println("Motion " + " X " + a_Data.GetX() + " Y "
        + a_Data.GetY());
  }

  public void PassiveMotionFunc(PassiveMotionData a_Data) {
    System.out.println("Motion " + " X " + a_Data.GetX() + " Y "
        + a_Data.GetY());
  }

  public void KeyboardFunc(KeyboardData a_Data) {
    System.out.println("Keyboard " + " Key " + a_Data.GetKey() + " X "
        + a_Data.GetX() + " Y " + a_Data.GetY());
  }

  public void ReshapeFunc(ReshapeData a_Data) {
    System.out.println("Reshape " + " X " + a_Data.GetX() + " Y "
        + a_Data.GetY());

  }

  public void DisplayFunc(DisplayData a_Data) {
    System.out.println("Display");
  }

  public void MenuFunc(glutWindow a_W, MenuData a_Data) {
    System.out.println("Menu");
  }
}
