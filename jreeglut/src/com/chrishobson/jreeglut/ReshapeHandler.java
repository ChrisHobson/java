package com.chrishobson.jreeglut;

/**
 * Java interface for handlers for glut reshape.
 * 
 * @author clh
 * 
 */
public interface ReshapeHandler {

  /**
   * This function is called by glut when the window changes shape / size that
   * this handler this handler is attached to. The window is passed to the
   * handler.
   * 
   * @param x
   * @param y
   */
  public void ReshapeFunc(ReshapeData a_Data);
}
