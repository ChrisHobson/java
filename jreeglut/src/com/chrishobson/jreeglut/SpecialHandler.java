package com.chrishobson.jreeglut;

/**
 * Java interface for handlers for glut Special Keyboard.
 * 
 * @author clh
 * 
 */
public interface SpecialHandler {

  /**
   * This function is called by glut when a special key is pressed in a window.
   * 
   * @param x
   * @param y
   */
  public void SpecialFunc(KeyboardData a_Data);
}
