package com.chrishobson.jreeglut;

/**
 * All handlers take a HandlerData or a class derived from it. The
 * base class stores the glutWindow involved in the event, derived classes
 * add more data specific to the event type.
 * 
 * @author clh
 * 
 */
public class DisplayData extends HandlerData {

  public DisplayData(glutWindow a_Wind) {
    super(a_Wind);
  }
}
