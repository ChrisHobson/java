package com.chrishobson.jreeglut;

/**
 * Data for Motion events.
 * 
 * @author clh
 * 
 */
public class ReshapeData extends XYData {

  public ReshapeData(
      glutWindow a_Wind, int a_X, int a_Y
  )
  {
    super(a_Wind, a_X, a_Y);
  }
}
