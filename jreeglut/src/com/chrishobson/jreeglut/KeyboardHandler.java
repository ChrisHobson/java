package com.chrishobson.jreeglut;

/**
 * Java interface for handlers for glut reshape.
 * 
 * @author clh
 * 
 */
public interface KeyboardHandler {

  /**
   * This function is called by glut when a key is pressed in a window.
   * 
   * @param x
   * @param y
   */
  public void KeyboardFunc(KeyboardData a_Data);
}
