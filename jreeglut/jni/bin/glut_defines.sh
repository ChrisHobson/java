#!/bin/sh
#
# Script to extract the #defines from glut and format
# them as java static variables.
#
grep '#define' freeglut/include/GL/freeglut_std.h | \
  grep '0x' \
| sed -e 's/#define/public static int/' \
      -e 's/ 0x/= 0x/' \
      -e 's/$/;/'
