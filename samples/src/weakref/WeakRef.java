package weakref;

import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;

interface IData
{
  
}

public class WeakRef {

  /**
   * @param args
   */
  public static void main(String[] args) {
    
    for(int i = 0; i < 1000; ++i) {
      //System.gc();
 
      new Data1("Data 1 " + i);
    
    }
    System.gc();
    
    WeakData.Release();
    

  }

}

class Data2 implements IData{
  Data2(String a_Data) {
    m_Data = new WeakData(a_Data, this);
  }
  
  String GetData()
  {
    return m_Data.GetData();
  }
  
  private WeakData m_Data;
}
class Data1 implements IData{
  Data1(String a_Data) {
    m_Data = new WeakData(a_Data, this);
  }
  
  String GetData()
  {
    return m_Data.GetData();
  }
  
  private WeakData m_Data;
}

class WeakData extends WeakReference<IData> 
{
  public WeakData(String a_String, IData a_Data) {
    super(a_Data, s_List);
    m_Data = a_String;
    m_Next = s_Head;
    s_Head = this;
  }
  
  String GetData() {
    return m_Data;
  }
  
  public static void Release() 
  {
    WeakData d;
    while((d = (WeakData)s_List.poll()) != null) {
      System.out.println("Got " + d.GetData());
    }
    
  }
  
  
  private String m_Data;
  private WeakData m_Next;
  
  
  private static ReferenceQueue<IData> s_List = 
      new ReferenceQueue<IData>();
  
  private static WeakData s_Head = null;
  
}
