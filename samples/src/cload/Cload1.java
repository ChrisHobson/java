package cload;

import java.io.File;
import java.io.FileInputStream;

public class Cload1 extends ClassLoader {

  protected Class<?> loadClass(String a_Name, boolean resolve) {

    Class<?> c = findLoadedClass(a_Name);
    try {
      if (c == null) {
        File f = new File("classes/"
            + a_Name.replace(".", "/").concat(".class"));
        System.out.println("Name = " + f.getName());
        if (f.exists()) {
          FileInputStream fs = new FileInputStream(f);
          long len = f.length();
          byte[] cdata = new byte[(int) len];
          fs.read(cdata);
          c = defineClass(a_Name, cdata, 0, (int) len);
          if(resolve) {
            resolveClass(c);
          }
        } else {
          c = findSystemClass(a_Name);
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
    return c;
  }
}
