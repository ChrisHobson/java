package cload;

public class AClass {
  public AClass() {
    BClass b = new BClass();
    if (b.getClass().getClassLoader() == getClass().getClassLoader()) {
      System.out.println("B loaded by same loader as A");
    } else {
      System.out.println("B NOT loaded by same loader as A");
    }
  }
  void fred() {
    int a;
  }

  public void RunMe() {
    System.out.println("I am running");
  }
  
  BClass GetB() {
    return new BClass();
  }
  
  static {
    System.out.println("Class AClass Loaded");
  }
}
