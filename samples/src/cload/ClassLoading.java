package cload;

import java.lang.reflect.Method;

/**
 * Example if class loading.
 * @author clh
 *
 */

public class ClassLoading {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.println("Class Loader");
		try {
		Cload1 one = new Cload1();
		Class<?> c1 = one.loadClass("cload.AClass");
		if(one == c1.getClassLoader()) {
		  System.out.println("Loaded by CLoad1");
		}
		Method m = c1.getDeclaredMethod("RunMe", (Class<?>[])null);
		Object a1 = c1.newInstance();
		m.invoke(a1, (Object[])null);
		
		Cload1 two = new Cload1();
		Class<?> c2 = two.loadClass("cload.AClass");
		if(c2.getClassLoader() == c1.getClassLoader()) {
      System.out.println("Same loaders");
		} else {
      System.out.println("Different loaders");
		}
		
		System.out.println("Loaded " + c1.getName());
		} catch(Exception e) {
			e.printStackTrace();
		}

	}

}
