package linkedlist;

/**
 * Sample of a LinkedList which uses a circular list to minimise the
 * need to check for empty lists when adding and deleting items.
 * When looping through the items you only need to check whether the
 * next item is the header item to know to end, no different to checking
 * for null when not having the circular list.
 * 
 * @author clh
 *
 */

public class LinkedList {

  /**
   * @param args
   */
  public static void main(String[] args) {
    // TODO Auto-generated method stub
    
    new LinkedList().Doit();
  }
  void Doit() {
    
    List l = new List();
    l.Append("One");
    l.Append("Two");
    l.Append("Three");
    l.Insert("abc");
    
    //l.RemoveFirst();
    ///l.RemoveLast();
    //l.RemoveLast();
    //l.RemoveLast();
    
    l.Print();
    l.PrintRev();
    
  }
    
  class List {
    public List()
    {
      // The header record is a normal entry with no object
      m_Head = new Entry(null, null, null);
      // Once created make it circular list, it points at itself
      // and then as items are added the pointers are changed
      m_Head.m_Next = m_Head.m_Prev = m_Head;
    }
    
    void Append(Object o) {
      InsertAfter(m_Head.m_Prev, o);
    }
    
    void Insert(Object o) {
      InsertAfter(m_Head, o);
      
    }
    
    /**
     * Insert the object after this specified entry
     * @param e
     * @param o
     */
    void InsertAfter(Entry after, Object o) {
      Entry e = new Entry(after.m_Next, after, o);
      after.m_Next.m_Prev = e;
      after.m_Next = e;
      
    }
    
    
    void Print()
    {
      for(Entry e = m_Head.m_Next; e != m_Head; e = e.m_Next) {
        System.out.println(e.m_O.toString());        
      }
    }
    
    void PrintRev()
    {
      for(Entry e = m_Head.m_Prev; e != m_Head; e = e.m_Prev) {
        System.out.println(e.m_O.toString());        
      }
    }
    
    void RemoveFirst()
    {
      Remove(m_Head.m_Next);
    }
    void RemoveLast()
    {
      Remove(m_Head.m_Prev);
    }
    
    void Remove(Entry e)
    {
      e.m_Prev.m_Next = e.m_Next;
      e.m_Next.m_Prev = e.m_Prev;
      e.m_Prev = e.m_Next = null;
    }
    
    
    
    private Entry m_Head;
    

    /**
     * An item in the list, is a previous and next pointer (to entry) and the
     * data object.
     * @author clh
     *
     */
    class Entry {
      Entry(Entry a_Next, Entry a_Prev, Object a_O) {
        m_Next = a_Next;
        m_Prev = a_Prev;
        m_O = a_O;
      }
      
      Entry m_Next;
      Entry m_Prev;
      Object m_O;
    }    
  }
  

}
