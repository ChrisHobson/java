// chris 29 Mar 2011
package xml;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

public class Sax {

  /**
   * @param args
   */
  public static void main(String[] args) {
    try {
      SAXParser p = SAXParserFactory.newInstance().newSAXParser();
      p.parse("res/a.xml", new H());
    } catch(Exception e) {
      e.printStackTrace();
    }

  }

}

class H extends DefaultHandler
{
  @Override
  public void startElement(
    String uri,
    String localName,
    String qName,
    Attributes attributes
  )throws SAXException
  {
    System.out.println("Start " + qName);
    int nAtt = attributes.getLength();
    for(int i = 0; i < nAtt; ++i) {
      System.out.println("  " + attributes.getQName(i) +
        "=\"" + attributes.getValue(i)    
      );
      
    }
  }
  @Override
  public void endElement(
    String uri,
    String localName,
    String qName
  )throws SAXException
  {
    System.out.println("End " + qName);
  }
  
  @Override
  public void characters(
    char[] ch,
    int start,
    int length
  ) throws SAXException
  {
    String s = new String(ch,start,length);
    System.out.println("Text " + start + " " + length + "="+ s);
  }
}
