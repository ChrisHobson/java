package sockets;

import java.net.ServerSocket;

public class Server {

  /**
   * @param args
   */
  public static void main(String[] args) {
    // TODO Auto-generated method stub 
    try {

      ServerSocket s = new ServerSocket(4444);

      SocketWrap sock = new SocketWrap(s.accept());
      sock.PutString("str"); 
      System.out.println("Connected");
      for(int i = 0; i < 10; ++i) {
        sock.PutString("" + i);
        Thread.sleep(1000);
      }
      sock.PutString("\n");
      System.out.println("Server Done");
      Thread.sleep(2000);
      
      
    } catch (Exception e) {
      e.printStackTrace();
    }

  }

}
