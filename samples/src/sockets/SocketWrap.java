package sockets;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

public class SocketWrap {
  public SocketWrap(Socket s) throws IOException {
    m_Sock = s;
    // Set the time out to 5 milliseconds so that the client doesn't
    // wait forever for some data that may never arrive
    //m_Sock.setSoTimeout(500);
    // Create the reading and writing streams as recommend in the
    // JDK online help
    m_out = new PrintWriter(m_Sock.getOutputStream());
    m_in = new BufferedReader(new InputStreamReader(m_Sock.getInputStream()));

  }
  
  public SocketWrap(String a_Inet, int port) throws UnknownHostException, IOException {
    this(new Socket(a_Inet, port));
  }
  
  String GetLine() {
    String s = null;
    try {
      s = m_in.readLine();
    } catch(SocketTimeoutException e) {
      System.out.println("Time out");
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    return s;
  }
  
  void PutString(String l_Str) {
    m_out.print(l_Str);
    m_out.flush();
  }
  
  
  
  Socket m_Sock;
  private BufferedReader m_in;
  private PrintWriter m_out;
}
