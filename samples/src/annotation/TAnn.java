/**
 * A simple test program which uses reflect to query the
 * annotations added to methods.
 * @author clh
 */
package annotation;
import java.lang.reflect.*;

public class TAnn {

  /**
   * The main method makes an instance of the class and then uses
   * reflection to query the annotation data.
   */
  public static void main(String[] args) {
    try {
      TAnn a = new TAnn();

      // Query the method as this is need to then query the annotation
      Method m = a.getClass().getMethod("Hello");
      // Query the annotation by type
      Ann ann = m.getAnnotation(Ann.class);
      // and print
      System.out.println("Annotation on Hello : Num = "+ann.id() + " " + ann.name());

      // Do exact the same for the second method, which shows
      // that the annotation is on a per method basis
      m = a.getClass().getMethod("Hello2");
      ann = m.getAnnotation(Ann.class);
      System.out.println("Annotation on Hello2: Num = "+ann.id() + " " + ann.name());

    } catch(Exception e) {
      e.printStackTrace();
    }
  }

  @Ann(id=10, name = "bill") 
  public void Hello()
  {
  }
  @Ann(id=11, name = "will")
  public void Hello2()
  {
  }
}
