/**
 * an example of an Annotation that allows meta-data to
 * be tagged on to methods, classes or member data.
 * @author clh 1 Apr 2011
 */
package annotation;

import java.lang.annotation.*;
/**
 * You can only query annotations if they are retained so as to be
 * available at runtime
 * @author clh
 *
 */
@Retention(RetentionPolicy.RUNTIME)
public  @interface Ann {
  int id();
  String name();
}
