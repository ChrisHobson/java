package com.chrishobson.maths;

/** Holds a 3D point in X,Y,Z space.
 *
 * that can be and is
 * also a wonderous creation
 */

public class Point3 {
  /** Construct a 3D point which is at the origin - 0,0,0
   */
  public Point3(){}

  /** Construct a 3D point which is at the same location as an existing Point3.
   */
  public Point3(Point3 p) {
    become(p);
  }

  /** Construct a 3D point at a given X,Y,Z.
   */
  public Point3(double x, double y, double z) {
    set(x,y,z);
  }

  /** Copies the location of a existing Point3.
   */
  void become(Point3 p) {
    m_xyz[0] = p.m_xyz[0];
    m_xyz[1] = p.m_xyz[1];
    m_xyz[2] = p.m_xyz[2];
  }
  
  /** Returns the internal array.
   */
  public double[] get_xyz() {
    return m_xyz; 
  }

  /** Get the X location.
   */
  public final double x() {
    return m_xyz[0];
  }
  /** Get the Y location.
   */
  public final double y() {
    return m_xyz[1];
  }
  /** Get the Z location.
   */
  public final double z() {
    return m_xyz[2];
  }

  /** Set all coordinate values in one go
   */
  public void set(double x, double y, double z) {
    m_xyz[0] = x;
    m_xyz[1] = y;
    m_xyz[2] = z;
  }

  /** Set a new X position.
   */
  public final void set_x(double x) {
    m_xyz[0] = x;
  }
  /** Set a new Y position.
   */
  public final void set_y(double y) {
    m_xyz[1] = y;
  }
  /** Set a new Z position.
   */
  public final void set_z(double z) {
    m_xyz[2] = z;
  }
  
  private double m_xyz [] = {0.0,0.0,0.0};
}
