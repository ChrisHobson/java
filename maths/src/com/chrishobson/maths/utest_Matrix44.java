package com.chrishobson.maths;

import com.chrishobson.maths.Matrix44;
import com.chrishobson.maths.Point3;

class utest_Matrix44 {
  public static void main(String args[]) {
    System.out.println("Matrix44 Test");
    Matrix44 m = new Matrix44();

    Point3 p1 = new Point3(1.0,2.0,3.0);
    m.make_transform(p1);

    Point3 p2 = new Point3(1.0,0.0,0.0);
    Matrix44 m2 = new Matrix44();
    m2.make_transform(p2);

    Matrix44 m3 = new Matrix44();
    m3.concat(m,m2);

    Point3 p = new Point3();
    m3.transform(p);

    System.out.println(p.x());
    System.out.println(p.y());
    System.out.println(p.z());

    m.make_rotation_z(90.0);
    m3.concat(m,m2);

    System.out.println(p2.x());
    System.out.println(p2.y());
    System.out.println(p2.z());

    m.transform(p2);

    System.out.println(p2.x());
    System.out.println(p2.y());
    System.out.println(p2.z());


  }
}
