package com.chrishobson.awt;

import java.awt.Graphics;
import java.awt.Color;
import com.chrishobson.maths.*;

/** A class which allows 3D drawing into a Graphics object.
 * Ramble, blah blah
 */

public class Graphics3D {
  public Graphics3D(Graphics g) {
    m_g = g;
  }
  public Graphics3D(Graphics g,
		   Matrix44 m) {
    m_g = g;
    m_m = new Matrix44(m);
  }
  public void set_matrix(Matrix44 m) {
    m_m.become(m);
  }
  public void set_graphics(Graphics g) {
    m_g = g;
  }

  /**
   * Draw line between two Point3D.
   */

  public void draw_line(Point3 from, Point3 to) {
    m_m.transform(from,m_p1);
    m_m.transform(to,m_p2);

    m_g.drawLine((int)m_p1.x(),(int)m_p1.y(),(int)m_p2.x(),(int)m_p2.y());
  }

  /**
   * Clear the graphics with the current background colour.
   */
  public void clear() {
    Color c = m_g.getColor();
    m_g.setColor(Color.black);
    m_g.fillRect(0, 0, 500, 500);
    m_g.setColor(c);
  }


  private Graphics m_g;
  private Matrix44 m_m = new Matrix44();
  private Point3 m_p1 = new Point3();
  private Point3 m_p2 = new Point3();
}
