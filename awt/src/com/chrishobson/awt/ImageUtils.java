package com.chrishobson.awt;

import java.awt.Image;
import java.awt.MediaTracker;
import java.awt.Component;
import java.awt.Graphics;

public class ImageUtils {
  public ImageUtils(Component c) {
    m_c = c;
  }

  public void waitForLoad(Image i) {
    try {
      // Use a media tracker to ensure the gif is fully load
      MediaTracker t = new MediaTracker(m_c);
      t.addImage(i,0);
      t.waitForID(0); 
    } catch(Exception e) {
      e.printStackTrace();
    }
  }

  public Image copyImage(Image i) {
    int w = i.getWidth(m_c);
    int h = i.getHeight(m_c);
    Image img = m_c.createImage(w,h);
    Graphics g = img.getGraphics();
    g.drawImage(i,0,0,m_c);
    g.dispose();
    return img;
  }

  private Component m_c;

}
