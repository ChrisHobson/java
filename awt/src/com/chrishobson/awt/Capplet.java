package com.chrishobson.awt;

import com.chrishobson.awt.ImageUtils;

import java.applet.Applet;
import java.awt.Image;
import java.applet.AudioClip;

/** A Capplet is the base for Chris's applets (hence Capplet)
 *
 */

@SuppressWarnings("serial")
public class Capplet extends Applet {
  public Capplet() {
    super();
    System.out.println(getClass().getName());
    System.out.println("An Applet by Chris Hobson");
    System.out.println("  chris@chrishobson.com");
  }
  public void debugMess(String s) {
    if(m_debug_print == 0) {
      m_debug_print = 1;
      if(getParameter("clhdebug") != null) {
	m_debug_print = 2;
      }
    }
    if(m_debug_print == 2) {
      print(s);
    }
  }

  public void print(String s) {
    System.out.println(s);
  }

  /** Utility to load the named image from the Applets Jar archive.
   * This method assumes that the Applet if stored in a Jar and that
   * the named image is also in the Jar. It loads the image and returns
   * it. Images in jars should be named with a leading / typically
   * an image will be called something like /images/fred.gif
   * the method returns null if the image cannot be loaded, it also
   * prints an error message via the print function.
   */
  public Image getJarImage(String name) {
    // Ensure that the Image Utils class has been made
    make_img_utils();
    Image img = null;
    try {
      // This code will very likely fail on netscape browsers
      // and one day I might do something clever to work around it
      // getResource is not support by netscape
      img = getImage(getClass().getResource(name));
      m_img_utils.waitForLoad(img);
    } catch(Exception e) {
      // To be on the safe side
      img = null;
    }
    if(img == null) {
      print("Failed to load image: " + name);
    }
    return img;
  }

  /** Utility to load the named sound from the Applets Jar archive.
   * This method assumes that the Applet if stored in a Jar and that
   * the named sound is also in the Jar. It loads the sound and returns
   * it. Sounds in jars should be named with a leading / typically
   * an image will be called something like /sounds/fred.au
   * the method returns null if the sound cannot be loaded, it also
   * prints an error message via the print function.
   */
  public AudioClip getJarSound(String name) {
    AudioClip clip = null;
    try {
      // This code will very likely fail on netscape browsers
      // and one day I might do something clever to work around it
      // getResource is not support by netscape
      clip = getAudioClip(getClass().getResource(name));
    } catch(Exception e) {
      // To be on the safe side
      clip = null;
    }
    if(clip == null) {
      print("Failed to load sound: " + name);
    }
    return clip;
  }

  /** Copies the given Image to a new Image created via createImage.
   * Using createImage allows a Graphics context to be obtained for
   * the image, an image loaded via such a route as getJarImage cannot
   * have a Graphics context created for it. I don't know why but the
   * call to getGraphics on an image not create via createImage fails.
   * This method simple wraps the ImageUtils.copyImage method but
   * its nicer to call this method in Applets.
   */
  public Image copyImage(Image i) {
    make_img_utils();
    return m_img_utils.copyImage(i);
  }

  private final void make_img_utils() {
    if(m_img_utils == null) {
      debugMess("Making an Image Utils class for the Applet");
      m_img_utils = new ImageUtils(this);
    }
  }

  private int m_debug_print;
  private ImageUtils m_img_utils;
}
