package com.chrishobson.pclass;

import java.io.DataInputStream;
import java.io.IOException;


/**
 * Constant Pool - Integer Entry
 * @author Chris Hobson
 *
 */
public class Const3 extends ConstItem
{
	public void Read(DataInputStream ds, SectConst sect) throws IOException {
		  m_Integer = ds.readInt();
		}
		
		double GetInteger() {
			return m_Integer;
		}
		
		public void Print() {
		  System.out.print(" Int " + m_Integer);
		}

		private int m_Integer;
}
