package com.chrishobson.pclass;

import java.io.DataInputStream;
import java.io.IOException;


/**
 * Constant Pool - Long Entry
 * @author Chris Hobson
 *
 */
public class Const5 extends ConstItem
{
	public void Read(DataInputStream ds, SectConst sect) throws IOException {
		  m_Long = ds.readLong();
		}
		
		double GetLong() {
			return m_Long;
		}
		
		public void Print() {
		  System.out.print(" Long " + m_Long);
		}

		private long m_Long;
}
