package com.chrishobson.pclass;

import java.io.DataInputStream;
import java.io.IOException;

public class SectVers extends Section {

	void Read(DataInputStream ds) throws IOException {
		m_Magic = ds.readInt();
		m_Minor = ds.readShort();
		m_Major = ds.readShort();

	}
	
	public int GetMagic() {
	  return m_Magic;
    }

	public int GetMajor() {
		return m_Major;
	}

	public int GetMinor() {
		return m_Minor;
	}

	int m_Magic;
	int m_Minor;
	int m_Major;
}
