package com.chrishobson.pclass;

/**
 * Constant Pool - Fieldref Entry
 * 
 * @author Chris Hobson
 * 
 */

public class Const11 extends ConstRef {
  Const11() {
    super("InterfaceMethodref");
  }
}
