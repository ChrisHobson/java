package com.chrishobson.pclass;

/**
 * Constant Pool - Methodref Entry
 * @author Chris Hobson
 *
 */

public class Const10 extends ConstRef {
  public Const10() {
    super("Methodref");
  }
}
