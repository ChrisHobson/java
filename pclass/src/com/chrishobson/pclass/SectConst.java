package com.chrishobson.pclass;

import java.io.DataInputStream;
import java.io.IOException;
import java.util.Vector;

import com.chrishobson.pclass.Section;

public class SectConst extends Section {
  void Read(DataInputStream ds) throws IOException, ClassNotFoundException, InstantiationException, IllegalAccessException {
	m_Num = ds.readShort() - 1;
	m_Items = new Vector<ConstItem>(m_Num);
	for(int i = 1; i <= m_Num; ++i) {
	  short type = ds.readByte();
	
	  String name = new String("com.chrishobson.pclass.Const" + type);
	  ConstItem item = (ConstItem)Class.forName(name).newInstance();
	  item.Read(ds, this);
	  m_Items.add(item);
	  if(type > 4 && type < 7) {
		  // long and double take two entries, for some silly reason
		  // when we build an array of items add a null in the second
		  // slot as that is not the valid index
		  ++i;
		  m_Items.add(null);
	  }
	}
	  
  }
  public int NumConst() {
	  return m_Num;
  }
  
  public ConstItem GetItemByIdx(int idx) {
	  return m_Items.elementAt(idx - 1);
  }
	
  public void Print() {
	  for(int i = 1; i <= m_Num; ++i) {
		  ConstItem item = GetItemByIdx(i);
		  if(item != null) {
			  System.out.print("#" + i);
			  item.Print();
			  System.out.println();
		  }
	  }
  }
  
  private int m_Num;
  Vector<ConstItem> m_Items;
}
