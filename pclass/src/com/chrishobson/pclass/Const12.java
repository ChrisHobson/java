package com.chrishobson.pclass;


/**
 * Constant Pool - NameAndType Entry
 * 
 * @author Chris Hobson
 *
 */
public class Const12 extends ConstIdx2 {
  public void Print() {
    super.Print();
    System.out.print(" NameAndType " + GetName() + " " + GetDesc());
    
  }
  public int GetNameIdx() {
    return GetIdx1();
  }
  public int GetDescIdx() {
    return GetIdx2();
  }
  public String GetName() {
    return ((Const1)m_Sect.GetItemByIdx(GetNameIdx())).GetStr();
  }
  public String GetDesc() {
    return ((Const1)m_Sect.GetItemByIdx(GetDescIdx())).GetStr();
  }
  
}
