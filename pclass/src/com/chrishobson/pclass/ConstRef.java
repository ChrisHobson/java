package com.chrishobson.pclass;

/**
 * Constant Pool base class for Methodref, Fieldref and InterfaceMethodref
 * items.
 * 
 * @author Chris Hobson
 *
 */
class ConstRef extends ConstIdx2 {
  ConstRef(String type) {
    m_Type = type;
  }

  public int ClassIdx() {
    return GetIdx1();
  }
  public int GetNameAndTypeIdx() {
    return GetIdx2();
  }
  public Const12 GetNameAndType() {
    return (Const12)m_Sect.GetItemByIdx(GetNameAndTypeIdx());
  }
  public String GetClassName() {
    return ((Const7)(m_Sect.GetItemByIdx(ClassIdx()))).GetClassName();
  }
  
  public void Print() {
    super.Print();
    Const12 nameandtype = GetNameAndType();
    System.out.print(" " + m_Type + " " + GetClassName() + " " + nameandtype.GetName() + " " + nameandtype.GetDesc());
  }	
	private String m_Type;
}
