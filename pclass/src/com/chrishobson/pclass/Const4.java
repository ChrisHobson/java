package com.chrishobson.pclass;

import java.io.DataInputStream;
import java.io.IOException;


/**
 * Constant Pool - Float Entry
 * @author Chris Hobson
 *
 */
public class Const4 extends ConstItem
{
	public void Read(DataInputStream ds, SectConst sect) throws IOException {
		  m_Float = ds.readFloat();
		}
		
		double GetFloat() {
			return m_Float;
		}

		public void Print() {
		  System.out.print(" Float " + m_Float);
		}
		
		private float m_Float;
}
