package com.chrishobson.pclass;

/**
 * Constant Pool - Fieldref Entry
 * 
 * @author Chris Hobson
 * 
 */

public class Const9 extends ConstRef {
  Const9() {
    super("Fieldref");
  }
}
