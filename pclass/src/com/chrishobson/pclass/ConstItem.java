package com.chrishobson.pclass;

import java.io.DataInputStream;
import java.io.IOException;

abstract public class ConstItem {
  abstract public void Read(DataInputStream ds, SectConst sect) throws IOException;
  
  public void Print() {
	  System.out.println(getClass().getName());
  }
}
