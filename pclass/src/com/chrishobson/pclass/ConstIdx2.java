package com.chrishobson.pclass;

import java.io.DataInputStream;
import java.io.IOException;

/**
 * Constant Pool base class for items with 2 indexes to other
 * constant pool items.
 * 
 * @author Chris Hobson
 *
 */
class ConstIdx2 extends ConstIdx1 {
	public void Read(DataInputStream ds, SectConst sect) throws IOException {
		super.Read(ds, sect);
	  m_Idx2 = ds.readShort();
	}
	
	short GetIdx2() {
		return m_Idx2;
	}
  public void Print() {
    super.Print();
    System.out.print(".#" + m_Idx2);
  }

	private short m_Idx2;
}
