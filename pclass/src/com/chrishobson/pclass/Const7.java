package com.chrishobson.pclass;

/**
 * Constant Pool - Class Entry
 * @author Chris Hobson
 *
 */

public class Const7 extends ConstIdx1 {
	public String GetClassName() {
		return ((Const1)m_Sect.GetItemByIdx(GetIdx1())).GetStr();
	}
	public void Print() {
	  super.Print();
		System.out.print(" Class " + GetClassName());
	}
}
