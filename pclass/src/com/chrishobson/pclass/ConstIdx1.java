package com.chrishobson.pclass;

import java.io.DataInputStream;
import java.io.IOException;

/**
 * Constant Pool base class for items with 1 index to another
 * constant pool item.
 * 
 * @author Chris Hobson
 *
 */
class ConstIdx1 extends ConstItem {
	public void Read(DataInputStream ds, SectConst sect) throws IOException {
	  m_Idx1 = ds.readShort();
	  m_Sect = sect;
	}
	
	short GetIdx1() {
		return m_Idx1;
	}

	public void Print() {
	  System.out.print(" #" + m_Idx1);
	}
	
	private short m_Idx1;
	protected SectConst m_Sect;
}
