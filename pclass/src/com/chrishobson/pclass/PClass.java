package com.chrishobson.pclass;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.util.Formatter;

public class PClass {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.println("Java Class Parser");
		
		try {
			FileInputStream fs = new FileInputStream("classes/com/chrishobson/pclass/PClass.class");
			DataInputStream ds = new DataInputStream(fs);
			Formatter fm = new Formatter();
			
			SectVers vers = new SectVers();
			vers.Read(ds);
			
			System.out.println(fm.format("Magic 0x%H Major = %d Minor = %d",
					vers.GetMagic(), vers.GetMajor(), vers.GetMinor()));
			
			SectConst cnst = new SectConst();
			cnst.Read(ds);
			cnst.Print();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	double x = 12.34;
	int y = 1233333333;
	float f = 12.234f;
	long l = 1234;
}
