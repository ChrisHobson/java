package com.chrishobson.pclass;

import java.io.DataInputStream;
import java.io.IOException;

/**
 * Constant Pool - UTF8 Entry
 * 
 * @author Chris Hobson
 * 
 */
public class Const1 extends ConstItem {
  public void Read(DataInputStream ds, SectConst sect) throws IOException {
    int len = ds.readShort();
    byte b[] = new byte[len];
    ds.read(b);
    m_Str = new String(b);
  }

  public String GetStr() {
    return m_Str;
  }

  public void Print() {
    System.out.print(" UTF8 //" + GetStr());
  }

  private String m_Str;
}
