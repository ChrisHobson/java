package com.chrishobson.pclass;

import java.io.DataInputStream;
import java.io.IOException;

/**
 * Constant Pool - Double Entry
 * 
 * @author Chris Hobson
 * 
 */
public class Const6 extends ConstItem {
	public void Read(DataInputStream ds, SectConst sect) throws IOException {
		m_Double = ds.readDouble();
	}

	double GetDouble() {
		return m_Double;
	}
	
	public void Print() {
		System.out.print(" Double " + GetDouble());
	}

	private double m_Double;
}
