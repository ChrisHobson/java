package com.chrishobson.pclass;


/**
 * Constant Pool - String Entry
 * @author Chris Hobson
 *
 */
public class Const8 extends ConstIdx1 {
  public String GetStr() {
    return ((Const1)(m_Sect.GetItemByIdx(GetIdx1()))).GetStr();
  }
  public void Print() {
    super.Print();
    System.out.println(" String //" + GetStr());
  }
}
