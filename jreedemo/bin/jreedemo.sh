#!/bin/sh
#
# Script to run the demo at work so others can see it
#
PATH="$PATH://machine27/c/Program Files/Java/jdk1.6.0_27/bin://machine27/c/Program Files/Java/apache-ant-1.8.2/bin"
type java
type ant
#if(test "$JAVA_HOME" = "") then
  export JAVA_HOME="//machine27/C/Program Files/Java/jdk1.6.0_27"
#fi
#if(test "$ANT_HOME" = "") then
  export ANT_HOME="//machine27/c/Program Files/Java/apache-ant-1.8.2"
#fi
  echo $JAVA_HOME
ant -f //machine27/c/projects/chrishobson-java/jreedemo/build.xml -Ddemo=First3D
