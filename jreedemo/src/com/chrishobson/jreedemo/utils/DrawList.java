// chris 22 Dec 2011
package com.chrishobson.jreedemo.utils;

import java.util.LinkedList;

/**
 * A LinkedList of drawables. This class is itself an IDrawable
 * which allows it to be added to other lists to build a tree.
 * The Draw method calls Draw on the contents.
 * @author clh
 *
 */

public class DrawList extends LinkedList<IDrawable> implements IDrawable {
  
  public void Draw(Renderer a_Render) {
    for(IDrawable l_Draw : this) {
      l_Draw.Draw(a_Render);
    }
  }
  
  public void Destroy() {
    
  }
  
  @Override
  public void Render(Renderer a_Renderer) {
  
  }
}