// chris 18 Dec 2011
package com.chrishobson.jreedemo.utils;

import static com.chrishobson.jreeglut.glut.*;

/**
 * Wrapper to glutCube.
 * 
 * @author clh
 * 
 */

public class glutCube extends DlDrawable {

  public glutCube(double size) {
    m_Size = size;
  }

  public void Render(Renderer a_Renderer) {

    glutWireCube(m_Size);
  }

  private double m_Size;
}
