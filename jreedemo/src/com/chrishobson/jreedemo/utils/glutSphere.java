// chris 18 Dec 2011
package com.chrishobson.jreedemo.utils;

import static com.chrishobson.jreeglut.glut.*;

/**
 * Wrapper to glutCube.
 * 
 * @author clh
 * 
 */

public class glutSphere extends DlDrawable {

  public glutSphere(double radius, int stacks, int segments) {
    m_Radius = radius;
    m_Stacks = stacks;
    m_Segs = segments;
  }

  public void Render(Renderer a_Renderer) {

    glutWireSphere(m_Radius, m_Stacks, m_Segs);
  }

  private double m_Radius;
  private int m_Stacks;
  private int m_Segs;
}
