// chris 12 Dec 2011
package com.chrishobson.jreedemo.utils;

import static com.chrishobson.jreeglut.glut.*;
import com.chrishobson.jreeglut.*;

import com.chrishobson.jreeglut.glut;

public class DemoBase {
  public DemoBase(String a_Title) {
    glut.glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
    m_W = new glutWindow(a_Title);
  }
  
  protected glutWindow m_W;
}
