// chris 20 Dec 2011
package com.chrishobson.jreedemo.utils;

/*
 * A base class of things that can be drawn, a Drawable issues OpenGL
 * commands when its Render method is called. The Render method must be
 * overriden by all derived objects, however Render is never directly
 * invoked, instead the Draw method is called by uses which routes to
 * Render.
 * 
 */

public class Renderer {
  
}
