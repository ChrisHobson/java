// chris 18 Dec 2011
package com.chrishobson.jreedemo.utils;

import static com.chrishobson.jreegl.gl.*;

/**
 * Draws a simple colour coded axes set.
 * 
 * @author clh
 * 
 */

public class SimpleAxes extends DlDrawable {

  public SimpleAxes(double size) {
    m_Size = size;
  }

  public void Render(Renderer a_Renderer) {
    // Red line along X axis
    glColor(1.0, 0.0, 0.0);
    glBegin(GL_LINES);
    {
      glVertex(0, 0, 0);
      glVertex(m_Size, 0, 0);
    }
    glEnd();

    // Green line along Y axis
    glColor(0.0, 1.0, 0.0);
    {
      glBegin(GL_LINES);
      glVertex(0, 0, 0);
      glVertex(0, m_Size, 0);
    }
    glEnd();

    // Blue line along Z axis
    glColor(0.0, 0.0, 1.0);
    {
      glBegin(GL_LINES);
      glVertex(0, 0, 0);
      glVertex(0, 0, m_Size);
    }
    glEnd();
  }

  private double m_Size;
}
