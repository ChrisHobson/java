// chris 20 Dec 2011
package com.chrishobson.jreedemo.utils;

/*
 * Interface for classes that can be drawn, a Drawable issues OpenGL
 * commands when its Render method is called. The Render method must be
 * overriden by all derived objects, however Render is never directly
 * invoked, instead the Draw method is called by users which routes to
 * Render.
 * 
 */

public interface IDrawable {
  
  public void Draw(Renderer a_Renderer);

  /**
   * Called when no longer required to allow any cached data
   * to be released. The finalize method will also call this but
   * it cannot be sure that the correct OpenGL context is active and
   * so its better to do it.
   */
  public void Destroy();

  
  void Render(Renderer a_Renderer);
}
