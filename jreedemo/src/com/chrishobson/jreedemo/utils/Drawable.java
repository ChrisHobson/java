// chris 20 Dec 2011
package com.chrishobson.jreedemo.utils;

/*
 * A base class of things that can be drawn, a Drawable issues OpenGL
 * commands when its Render method is called. The Render method must be
 * overriden by all derived objects, however Render is never directly
 * invoked, instead the Draw method is called by users which routes to
 * Render.
 * 
 */

abstract public class Drawable implements IDrawable {
  
  public void Draw(Renderer a_Renderer) {
    Render(a_Renderer);
  }
 
  public abstract void Render(Renderer a_Renderer);
}
