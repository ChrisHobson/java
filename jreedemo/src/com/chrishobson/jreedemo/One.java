package com.chrishobson.jreedemo;

import com.chrishobson.jreeglut.*;

import static  com.chrishobson.jreegl.gl.*;

/**
 * Absolute minimal jreeglut/jreegl test program. 
 * <p>
 * This is based on the first program in the OpenGL redbook
 * the fact it runs shows everything is setup and working.
 * <p>
 * The program draws a filled white square in the midddle
 * of a black window. The program is slightly more advanced that
 * the redbook in that it handles refreshes by reseting the
 * viewing matrix, as explained in the redbook as it
 * moves on to describe the first program
 * 
 * @author clh
 *
 */
public class One {

  public One(
    String[] a_Args
  )
  {
    glutWindow l_W = new glutWindow("Demo One");
    l_W.SetDisplayHandler(new WindowHandler());
  }
  
}

class WindowHandler implements DisplayHandler
{
  public void DisplayFunc(DisplayData a_Data) {
    glClearColor (0.0,0.0,0.0, 0.0);
    glClear (GL_COLOR_BUFFER_BIT);
    glColor(1.0, 1.0, 1.0);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0.0, 1.0, 0.0, 1.0, -1.0, 1.0);
    glBegin(GL_POLYGON);
      glVertex (0.25, 0.25, 0.0);
      glVertex (0.75, 0.25, 0.0);
      glVertex (0.75, 0.75, 0.0);
      glVertex (0.25, 0.75, 0.0);
    glEnd();
    glFlush(); 
  }
}
