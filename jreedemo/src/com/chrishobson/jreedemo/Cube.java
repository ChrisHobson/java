package com.chrishobson.jreedemo;

import com.chrishobson.jreeglut.*;

import static  com.chrishobson.jreeglut.glut.*;
import static  com.chrishobson.jreegl.gl.*;


/**
 * Cube demo from redbook. 
 * <p>
 * Draws in a perspective view but which has a scale along y so
 * does not appear as a cube, unless the aspect ratio of the
 * screen is changed by resizing it.
 * <p>
 * Note that I have changed this demo to be double buffered so
 * that dragging the window size gives smooth redrawing, assuming
 * windows is setup to redraw the contents during a drag.
 * 
 * @author clh
 *
 */
public class Cube {
  static void init() {
    glClearColor(0.0, 0.0, 0.0, 0.0);
    glShadeModel(GL_FLAT);
  }

  public Cube(
    String[] a_Args
  )
  {
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
    glutInitWindowSize(500, 500);
    glutInitWindowPosition(100, 100);
    glutWindow w = new glutWindow("Cube Demo");
    init();
    CubeHandler wh = new CubeHandler();
    w.SetDisplayHandler(wh);
    w.SetReshapeHandler(wh);
    w.SetMouseHandler(wh);
  }
  
}

class CubeHandler implements DisplayHandler, ReshapeHandler, MouseHandler
{
  public void DisplayFunc(DisplayData a_Data)
  {
    glClear(GL_COLOR_BUFFER_BIT);
    glColor(1.0, 1.0, 1.0);
    glLoadIdentity(); /* clear the matrix */
    /* viewing transformation */
    gluLookAt(0.0, 0.0, 5.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);
    glScale(1.0, 2.0, 1.0); /* modeling transformation */
    glutWireCube(1.0);
    glFlush();
    glutSwapBuffers();
  }
  public void ReshapeFunc(ReshapeData a_Data) {
    glViewport(0, 0, a_Data.GetX(), a_Data.GetY());
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glFrustum(-1.0, 1.0, -1.0, 1.0, 1.5, 20.0);
    glMatrixMode(GL_MODELVIEW);
  }
  public void MouseFunc(MouseData a_Data){
    System.out.println("Mouse " + a_Data.GetBut() + " State " + a_Data.IsDown() + " Pos " + a_Data.GetX() + " " + a_Data.GetY());
  }
}
