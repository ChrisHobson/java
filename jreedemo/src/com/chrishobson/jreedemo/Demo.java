package com.chrishobson.jreedemo;

import java.lang.reflect.Constructor;

import static  com.chrishobson.jreeglut.glut.*;


/**
 * Driver class for all Demos. The class provides the entry
 * point and creates class instance that is a Demo and
 * then invokes the Demo method on that instance.
 * <p>
 * glut is initialisaed before the Demo is constructed and
 * after the constructor is compled glutMainLoop is entered. 
 * 
 * @author clh
 *
 */
public class Demo {

  public static void main(
    String[] a_Args
  )
  {
    glutInit(a_Args);

    String l_ClassName = "com.chrishobson.jreedemo." + a_Args[0];
    
    try {
      Class<?> l_Class = Class.forName(l_ClassName);
      Constructor<?> l_Cons = l_Class.getConstructor(String[].class);
      l_Cons.newInstance((Object)a_Args);
      
      glutMainLoop();
    } catch(Exception e) {
      e.printStackTrace();
    }
  }
}
