// ============================================================================
// com.chrishobson.crossit.CrossMessage
// An interface 'Message' used by the game to output information messages
//
// History.
// -------
// CLH 01/01/99 Written
// ============================================================================

package com.chrishobson.crossit;

public interface CrossMessage {
  void message(String m);
  // Display the message string

  void win();
  // Play has won the game, the implemenation can do anything
  // it likes, such as play a tune
}
       
