// ============================================================================
// com.chrishobson.crossit.CrossitControl
// An interface 'Control' is used to specify things which a control in
// the Crossit game must be able to do.
//
// History.
// -------
// CLH 01/01/99 Written
// ============================================================================

package com.chrishobson.crossit;

public interface CrossControl {
  boolean state();  
  // Returns the current state True = On, False = Off
  
  void state(boolean s); 
  // Set control to the specified state

  void toggle();
  // The control should toggle states

  void off();
  // Switch off the control

  void on();
  // Switch on the control

  void hit();
  // Control has been hit by the user. The control should inform
  // the gaming engine
}
       
