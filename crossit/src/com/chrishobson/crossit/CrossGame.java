// ============================================================================
// com.chrishobson.crossit.CrossGame
// The class controls the game play. It requires that CrossControl items
// have been made and given to the CrossGame so that there is a level
// of abstract between the gaming engine and the physical game display
//
// The last game in the m_games array is a special game not in the LightsOut
// original. It lights all 25 buttons, I've seen other Java games which
// play like this so I've added it as a special varinat.
//
// History.
// -------
// CLH 01/01/99 Written
// CLH 17/01/99 Alter game array to store button numbers instead of X,Y
//              coordinates. This saves space and make it easier to add games
// ============================================================================

package com.chrishobson.crossit;

public class CrossGame {
  public final static int g_x = 5;
  public final static int g_y = 5;
  public CrossGame() {
  }

  public void set_controls(
			   CrossMessage m,
			   CrossControl c[][]
			   ) {
    m_controls = c;
    m_message = m;
  }

  public void hit(int x, int y) {
    for(int i = 0; i < 9;) {
      x += m_offsets[i++];
      y += m_offsets[i++];
      if(x >= 0 && x < g_x && y >= 0 && y < g_y) {
	m_controls[x][y].toggle();
      }
    }
    ++m_moves;
    status();
  }

  public void start() {
    start_game();
  }

  public void option(int opt) {
    m_option = opt;
    start_game();
  }


  private void start_game() {
    clear_board();
    m_moves = 0;
    int g[] = m_games[m_option - 1];
    int len = g.length;
    for(int i = 0; i < len; ++i) {
      int y = (g[i] - 1) / 5;
      int x = g[i] - 1 - 5 * y;
      m_controls[x][y].on();
    }
    status();
  }

  private void clear_board() {
    for(int x = 0; x < g_x; ++x) {
      for(int y = 0; y < g_y; ++y) {
	m_controls[x][y].off();
      }
    }
  }

  private int num_on() {
    int num = 0;
    for(int x = 0; x < g_x; ++x) {
      for(int y = 0; y < g_y; ++y) {
	if(m_controls[x][y].state()) {
	  ++num;
	}
      }
    }
    return num;
  }

  private void status() {
    if(m_option < g_num) {
      int min = (m_option + 4)/5 + 5;
      m_message.message(m_moves + " Moves (Min = " + min + ")");
      if(m_moves == min && num_on() == 0) {
	m_message.win();
      }
    } else {
      m_message.message(m_moves + " Moves");
    }
  }

  private CrossControl m_controls [][];
  private CrossMessage m_message;
  private static final int m_offsets[] = {0,0,-1,0,1,1,0,-2,1,1};
  private int m_option = 1;
  private int m_moves = 0;

  private static final int m_games[][] = {
    {11,13,15},
    {21,16,6,1,23,18,8,3,25,20,10,5},
    {16,11,6,22,17,12,7,2,24,19,14,9,4,20,15,10},
    {21,16,6,22,7,24,9,25,20,10},
    {21,11,6,1,22,12,7,2,13,8,3,24,19,4,25,20,15,10},

    {16,11,22,23,18,13,24,20,15},
    {21,16,11,6,1,22,2,23,3,24,4,20,15,10},
    {16,22,12,18,8,24,14,20},
    {21,6,22,17,12,7,2,23,13,8,19,14,9,4,20,10},
    {12,7,2,13,8,3,14,9,4},

    {16,11,6,1,22,23,18,13,8,3,24,20,15,10,5},
    {11,1,22,17,12,7,2,18,3,24,19,14,9,4,15,5},
    {16,22,12,18,8,14,4,10},
    {22,17,12},
    {17,7},

    {21,16,11,6,1,22,23,24,25},
    {21,22,17,23,18,13,24,19,25},
    {11,17,7,23,13,3,19,9,15},
    {21,11,1,23,13,3,25,15,5},
    {11,15},

    {22,17,12,7,2,13,3,14,4,5},
    {16,11,6,22,2,23,3,24,4,20,15,10},
    {23,18,13,19,14,15},
    {16,11,22,17,18,19,25,20,15}


    // Should always be the last game (Full board)
    ,{1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}
  };
  public final static int g_num = m_games.length;
}
       
