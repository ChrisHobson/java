package com.chrishobson.draw;



import java.awt.*;
import java.applet.*;
import java.util.Vector;

import com.chrishobson.awt.*;

class circle {
  public circle() {
    m_a = new Point();
    m_b = new Point();
  }
  public circle(Point a, Point b) {
    m_a = new Point(a);
    m_b = new Point(b);
  }
  public circle(circle c) {
    m_a = new Point(c.m_a);
    m_b = new Point(c.m_b);
  }
  public void set(Point a, Point b) {
    m_a.setLocation(a);
    m_b.setLocation(b);
  }

  public void draw(Graphics g) {
    int x = m_a.x < m_b.x ? m_a.x : m_b.x;
    int y = m_a.y < m_b.y ? m_a.y : m_b.y;
    int width = Math.abs(m_a.x - m_b.x);
    int height = Math.abs(m_a.y - m_b.y);

    g.drawOval(x,y,width,height);
  }

  private Point m_a;
  private Point m_b;
}

class band extends RubberBand {
  public band(Rubber d) {
    super(d);
    m_d = d;
  }
  protected void draw(Graphics g) {
    m_circle.set(m_anchor,m_last);
    m_circle.draw(g);
  }
  protected void ended() {
    m_d.add(m_circle);
  }

  private Rubber m_d;
  private circle m_circle = new circle();
}

@SuppressWarnings("serial")
public class Rubber extends Applet {

  public void init() {
    new band(this);
  }
  public void paint(Graphics g) {
    g.setColor(Color.red);
    System.out.println("Paint");
    int n = m_items.size();
    for(int i =0; i < n; ++i) {
      ((circle)m_items.elementAt(i)).draw(g);
    }
  }
  void add(circle c) {
    circle cc = new circle(c);
    m_items.addElement(cc);
    Graphics g = getGraphics();
    g.setColor(Color.red);
    cc.draw(g);
    g.dispose();
  }

  private Vector<circle>m_items = new Vector<circle>();

}
  
/*
<applet 
  code=com.chrishobson.draw.Rubber 
  width=500 
  height=500
>
</applet>
*/
