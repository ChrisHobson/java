package com.chrishobson.draw;


import com.chrishobson.awt.*;
import com.chrishobson.maths.*;

import java.awt.*;

public class Spin extends Capplet implements Runnable {

  public void update(Graphics g) {
  }
  public void paint(Graphics g) {
  }
  public void do_image() {
    if(m_g == null) {
      m_g = getGraphics();
    }
    if(m_g != null) {
      m_g.drawImage(m_img,0,0,this);
    }
  }
  
  public void init() {
    debugMess("In Init");
    m_img = createImage(300,300);
    m_gi = m_img.getGraphics();
    m_spin = new Thread(this,"Spin Thread");
  }

  public void start() {
    debugMess("In Start");
    if(m_started == false) {
      m_spin.start();
      m_started = true;
    } else {
      m_spin.resume();
    }
  }

  public void stop() {
    debugMess("In Stop");

    m_spin.suspend();
  }

  public void destroy() {
    debugMess("In Destroy");
    //    m_spin.stop();
    if(m_gi != null) {
      m_gi.dispose();
    }
    if(m_g != null) {
      m_g.dispose();
    }
  }

  public void run() {
    debugMess("In Run");

    Matrix44 mz = new Matrix44();
    mz.make_rotation_z(1);
    Matrix44 my = new Matrix44();
    my.make_rotation_y(10);
    Matrix44 mx = new Matrix44();
    mx.make_rotation_x(5);
    Matrix44 ms = new Matrix44();
    ms.make_scale(2.0);
    
    Matrix44 to_orig = new Matrix44();
    to_orig.make_transform(new Point3(-75.0,-75,-10.0));

    Matrix44 from_orig = new Matrix44();
    from_orig.make_transform(new Point3(150.0,150.0,0.0));
    
    Matrix44 rots = new Matrix44();
    Matrix44 curr = new Matrix44();

    Point3 p1 = new Point3(50.0,50.0,0.0);
    Point3 p2 = new Point3(100.0,50.0,0.0);
    Point3 p3 = new Point3(100.0,100.0,0.0);
    Point3 p4 = new Point3(50.0,100.0,0.0);

    Point3 p5 = new Point3(50.0,50.0,20.0);
    Point3 p6 = new Point3(100.0,50.0,20.0);
    Point3 p7 = new Point3(100.0,100.0,20.0);
    Point3 p8 = new Point3(50.0,100.0,20.0);

    Point3 p9 = new Point3(75.0,75.0,0.0);
    Point3 p10 = new Point3(75.0,75.0,20.0);
    Point3 p11 = new Point3(50.0,75.0,10.0);
    Point3 p12 = new Point3(100.0,75.0,10.0);
    Point3 p13 = new Point3(75.0,50.0,10.0);
    Point3 p14 = new Point3(75.0,100.0,10.0);

    Graphics3D g3 = new Graphics3D(m_gi,curr);
    while(true) {
      rots.concat(mz);
      rots.concat(my);
      rots.concat(mx);
      curr.make_identity();
      curr.concat(to_orig);
      curr.concat(ms);
      curr.concat(rots);
      curr.concat(from_orig);
      
      g3.set_matrix(curr);
      g3.clear();
      m_gi.setColor(Color.red);
      g3.draw_line(p1,p2);
      g3.draw_line(p2,p3);
      g3.draw_line(p3,p4);
      g3.draw_line(p4,p1);
      
      m_gi.setColor(Color.green);
      g3.draw_line(p5,p6);
      g3.draw_line(p6,p7);
      g3.draw_line(p7,p8);
      g3.draw_line(p8,p5);
      
      m_gi.setColor(Color.blue);
      g3.draw_line(p1,p5);
      g3.draw_line(p2,p6);
      g3.draw_line(p3,p7);
      g3.draw_line(p4,p8);
      
      m_gi.setColor(Color.yellow);
      g3.draw_line(p9,p10);
      g3.draw_line(p11,p12);
      g3.draw_line(p13,p14);
      
      do_image();
      try {
	Thread.sleep(50);
      } catch(Exception e) {
      }
      
    }
  }

  private Image m_img;
  private Graphics m_gi;
  private Graphics m_g;
  private Thread m_spin;
  private boolean m_started = false;
}
