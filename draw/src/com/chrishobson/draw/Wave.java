package com.chrishobson.draw;


import com.chrishobson.awt.Capplet;

import java.awt.*;

public class Wave extends Capplet implements Runnable {

  public void paint(Graphics g) {
    if(m_img != null) {
      g.drawImage(m_img,0,0,this);
    }
  }
  
  public void init() {
    debugMess("In Init, codeBase = " + getCodeBase());
    m_img = copyImage(getJarImage(getParameter("image")));

    new Thread(this).start();
  }

  public void run() {
    debugMess("In Run");
    Graphics gi = m_img.getGraphics();
    Graphics g = getGraphics();
    int x =1;
    int y = 2;
    while(true) {
      g.drawImage(m_img,0,0,this);
      try {
	Thread.sleep(500);
      } catch(Exception e) {
	e.printStackTrace();
      }
      gi.copyArea(x++,y++,5,5,3,3);
    }
  }

  private Image m_img;
}
